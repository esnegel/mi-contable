package com.snegrete.micontable.enumerator;

import java.util.stream.Stream;

public enum MonthEnum {

    ENERO(1, "Enero"),
    FEBRERO(2, "Febrero"),
    MARZO(3, "Marzo"),
    ABRIL(4, "Abril"),
    MAYO(5, "Mayo"),
    JUNIO(6, "Junio"),
    JULIO(7, "Julio"),
    AGOSTO(8, "Agosto"),
    SEPTIEMBRE(9, "Septiembre"),
    OCTUBRE(10, "Octubre"),
    NOVIEMBRE(11, "Noviembre"),
    DICIEMBRE(12, "Diciembre");

    private int num;
    private String name;

    MonthEnum(int num, String name){
        this.num = num;
        this.name = name;
    }

    public static int getMonthNumber(String name){
        return Stream.of(MonthEnum.values())
                .filter(monthEnum -> monthEnum.name.equalsIgnoreCase(name))
                .map(monthEnum -> monthEnum.num)
                .findFirst().orElse(0);
    }

    public static String getMonthName(int num){
        return Stream.of(MonthEnum.values())
                .filter(monthEnum -> monthEnum.num == num)
                .map(monthEnum -> monthEnum.name)
                .findFirst().orElse("");
    }

}
