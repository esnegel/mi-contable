package com.snegrete.micontable.common.configurationbean.textview;

public class TextViewInfoDTO {

    private Integer idText;
    private String text;

    public TextViewInfoDTO(Integer idText) {
        this.idText = idText;
    }

    public TextViewInfoDTO(Integer idText, String text) {
        this.idText = idText;
        this.text = text;
    }

    public Integer getIdText() {
        return idText;
    }

    public String getText() {
        return text;
    }
}
