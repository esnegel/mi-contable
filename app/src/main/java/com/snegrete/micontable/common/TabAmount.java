package com.snegrete.micontable.common;

import com.snegrete.micontable.model.AmountInfo;
import com.snegrete.micontable.model.DateInfo;

import java.util.List;

public class TabAmount<DateInfo, T> {

    private DateInfo dateInfo;
    private List<T> amountInfoList;

    public TabAmount(DateInfo dateInfo, List<T> amountInfoList) {
        this.dateInfo = dateInfo;
        this.amountInfoList = amountInfoList;
    }

    public DateInfo getDateInfo() {
        return dateInfo;
    }

    public void setDateInfo(DateInfo dateInfo) {
        this.dateInfo = dateInfo;
    }

    public List<T> getAmountInfoList() {
        return amountInfoList;
    }

    public void setAmountInfoList(List<T> amountInfoList) {
        this.amountInfoList = amountInfoList;
    }
}
