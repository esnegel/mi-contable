package com.snegrete.micontable.common.configurationbean.spinner;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.List;

public class SpinnerConfiguratorBean {

    private int id;
    private View view;
    private SpinnerListDTO list;
    private int selected;

    public SpinnerConfiguratorBean(int id, View view, SpinnerListDTO list, Integer selected) {
        this.id = id;
        this.view = view;
        this.list = list;
        this.selected = selected == null ? 0 : selected;
    }

    public Spinner configure(){
        Spinner spinner = view.findViewById(id);
        spinner.setAdapter(createAdapter());
        spinner.setSelection(selected);

        return spinner;
    }

    public ArrayAdapter<?> createAdapter(){
        if(list.get() instanceof Integer){
            return ArrayAdapter.createFromResource(view.getContext(),
                    (Integer) list.get(), android.R.layout.simple_spinner_dropdown_item);
        } else {
            return new ArrayAdapter<String>(view.getContext(),
                    android.R.layout.simple_spinner_dropdown_item, (List<String>) list.get());
        }

    }
}
