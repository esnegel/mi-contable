package com.snegrete.micontable.common.configurationbean.spinner;

import java.util.List;

public class SpinnerListDTO {

    private List<String> list;
    private Integer referenceId;

    public SpinnerListDTO(List<String> list) {
        this.list = list;
    }

    public SpinnerListDTO(Integer referenceId) {
        this.referenceId = referenceId;
    }

    public Object get() {
        if (list == null) {
            return referenceId;
        }
        return list;
    }
}
