package com.snegrete.micontable.common.configurationbean.textview;

import android.view.View;
import android.widget.TextView;

import java.util.List;

public class TextViewConfiguratorBean {

    private View view;
    private TextViewInfoDTO textViewInfo;

    public TextViewConfiguratorBean(View view) {
        this.view = view;
    }

    public TextViewConfiguratorBean(View view, TextViewInfoDTO textViewInfo) {
        this.view = view;
        this.textViewInfo = textViewInfo;
    }

    public TextView configure(){
        TextView textView = view.findViewById(textViewInfo.getIdText());
        textView.setText(textViewInfo.getText());
        return textView;
    }

    public TextView configure(TextViewInfoDTO textViewInfo){
        TextView textView = view.findViewById(textViewInfo.getIdText());
        textView.setText(textViewInfo.getText());
        return textView;
    }

    public void configureAll(List<TextViewInfoDTO> textViews){
        textViews.forEach(tv -> configure(tv));
    }
}
