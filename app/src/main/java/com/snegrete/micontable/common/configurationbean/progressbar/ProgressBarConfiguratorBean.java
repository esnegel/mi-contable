package com.snegrete.micontable.common.configurationbean.progressbar;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import com.snegrete.micontable.R;
import com.snegrete.micontable.common.configurationbean.textview.TextViewConfiguratorBean;

public class ProgressBarConfiguratorBean {

    private View view;
    private ProgressBarInfoDTO progressBarInfo;
    private Drawable background;
    private Drawable progress;

    public ProgressBarConfiguratorBean(Fragment fragment) {
        this.view = fragment.getView();
        setDrawables(fragment);
    }

    public ProgressBarConfiguratorBean(Fragment fragment, ProgressBarInfoDTO progressBarInfo) {
        this.view = fragment.getView();
        this.progressBarInfo = progressBarInfo;
        setDrawables(fragment);
    }

    private void setDrawables(Fragment fragment){
        background = ResourcesCompat.getDrawable(fragment.getResources(), R.drawable.progress_bar_background, null);
        progress = ResourcesCompat.getDrawable(fragment.getResources(), R.drawable.progress_bar_current, null);
    }

    public void setBackground(Drawable background) {
        this.background = background;
    }

    public void setProgress(Drawable progress) {
        this.progress = progress;
    }

    public ProgressBar configure(){
        return configure(progressBarInfo);
    }

    public ProgressBar configure(ProgressBarInfoDTO progressBarInfo){
        ProgressBar progressBar = view.findViewById(progressBarInfo.getIdProgressBar());

        if(progressBarInfo.configurable()){
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setBackground(background);
            progressBar.setProgressDrawable(progress);
            progressBar.setMax(progressBarInfo.getMax());
            progressBar.setProgress(progressBarInfo.getCurrent());
            if(progressBarInfo.withTextInfo()){
                new TextViewConfiguratorBean(view, progressBarInfo.getTextViewInfo()).configure();
            }
        } else {
            progressBar.setVisibility(View.GONE);
            if(progressBarInfo.withTextInfo()){
                view.findViewById(progressBarInfo.getTextViewInfo().getIdText()).setVisibility(View.INVISIBLE);
            }
        }

        return progressBar;
    }
}
