package com.snegrete.micontable.common.repository;

import com.snegrete.micontable.model.DateInfo;

import java.util.List;
import java.util.stream.Collectors;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

public class RealmRepository<T extends RealmObject> {

    private Class<T> type;

    public RealmRepository(Class<T> clazz) {
        this.type = clazz;
    }

    public void save(RealmObject obj){
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(obj);
            }
        });
        realm.close();
    }

    public void delete(Long id){
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(type).equalTo("id", id).findFirst().deleteFromRealm();
            }
        });
        realm.close();
    }

    public T findById(Long id) {
        Realm realm = Realm.getDefaultInstance();
        T ret = realm.where(type).equalTo("id", id).findFirst();
        realm.close();
        return ret;
    }

    public List<T> findAll() {
        Realm realm = Realm.getDefaultInstance();
        List<T> ret = realm.where(type).findAll().stream().collect(Collectors.toList());
        realm.close();
        return ret;
    }

    public RealmResults<T> findByDate(DateInfo dateInfo){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<T> ret = realm.where(type)
                .equalTo("month", dateInfo.getMonth())
                .equalTo("year", dateInfo.getYear())
                .findAll();
        realm.close();

        return ret;
    }
}
