package com.snegrete.micontable.common.configurationbean.progressbar;

import com.snegrete.micontable.common.configurationbean.textview.TextViewInfoDTO;

public class ProgressBarInfoDTO {

    private TextViewInfoDTO textViewInfo;
    private int idProgressBar;
    private Integer max;
    private Integer current;

    public ProgressBarInfoDTO(int idProgressBar) {
        this.idProgressBar = idProgressBar;
    }

    public ProgressBarInfoDTO(TextViewInfoDTO textViewInfo, int idProgressBar) {
        this.textViewInfo = textViewInfo;
        this.idProgressBar = idProgressBar;
    }

    public ProgressBarInfoDTO(int idProgressBar, int max, int current) {
        this.idProgressBar = idProgressBar;
        this.max = max;
        this.current = current;
    }

    public ProgressBarInfoDTO(int idProgressBar, int max, int current, TextViewInfoDTO textViewInfo) {
        this.textViewInfo = textViewInfo;
        this.idProgressBar = idProgressBar;
        this.max = max;
        this.current = current;
    }

    public boolean configurable() {
        return max != null && current != null;
    }

    public boolean withTextInfo(){
        return textViewInfo != null;
    }

    public TextViewInfoDTO getTextViewInfo() {
        return textViewInfo;
    }

    public int getIdProgressBar() {
        return idProgressBar;
    }

    public Integer getMax() {
        return max;
    }

    public Integer getCurrent() {
        return current;
    }
}
