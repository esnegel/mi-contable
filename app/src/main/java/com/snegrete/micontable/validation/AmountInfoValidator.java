package com.snegrete.micontable.validation;

import com.snegrete.micontable.R;
import com.snegrete.micontable.model.Ahorro;
import com.snegrete.micontable.model.AmountInfo;
import com.snegrete.micontable.model.DateInfo;
import com.snegrete.micontable.model.Gasto;
import com.snegrete.micontable.model.Ingreso;
import com.snegrete.micontable.ui.resumen.ahorros.service.AhorroService;
import com.snegrete.micontable.utilities.CalendarUtils;

public class AmountInfoValidator {

    private static AmountInfoValidator amountInfoValidator;

    private Integer errorMsgId;

    private AhorroService ahorroService = AhorroService.getInstance();

    private AmountInfoValidator(){}

    public static AmountInfoValidator getInstance(){
        if(amountInfoValidator == null){
            amountInfoValidator = new AmountInfoValidator();
        }
        return amountInfoValidator;
    }

    public boolean validate(Ingreso ingreso){
        errorMsgId = null;
        if(ingreso.getAmount().isNaN()){
            errorMsgId = R.string.ingreso_sin_cantidad_error;
        } else if("".equals(ingreso.getConcepto())){
            errorMsgId = R.string.importe_sin_concepto_error;
        }
        return errorMsgId == null;
    }

    public boolean validate(Gasto gasto){
        errorMsgId = null;
        if(gasto.getAmount().isNaN()){
            errorMsgId = R.string.gasto_sin_cantidad_error;
        } else if("".equals(gasto.getConcepto())){
            errorMsgId = R.string.importe_sin_concepto_error;
        }
        return errorMsgId == null;
    }

    public boolean validate(Ahorro ahorro){
        errorMsgId = null;
        if(ahorro.getAmount().isNaN()){
            errorMsgId = R.string.ahorro_sin_cantidad_error;
        }else if (ahorroService.findAhorro(ahorro) != null) {
            errorMsgId = R.string.ahorro_existente;
        }else if(isFutureDate(ahorro)){
            errorMsgId = R.string.ahorro_futuro_error;
        }
        return errorMsgId == null;
    }

    public boolean validateEdit(Ahorro ahorro){
        errorMsgId = null;
        if(ahorro.getAmount().isNaN()){
            errorMsgId = R.string.ahorro_sin_cantidad_error;
        }else if(isFutureDate(ahorro)){
            errorMsgId = R.string.ahorro_futuro_error;
        }
        return errorMsgId == null;
    }

    private boolean isFutureDate(AmountInfo amountInfo) {
        return new DateInfo(
                CalendarUtils.getActualMonth(),
                CalendarUtils.getActualYear().toString())
                .compareTo(amountInfo.getDateInfo()) < 0;
    }

    public int getErrorMsgId() {
        return errorMsgId;
    }
}
