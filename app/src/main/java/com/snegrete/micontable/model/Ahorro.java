package com.snegrete.micontable.model;

import androidx.annotation.Nullable;

public class Ahorro extends AmountInfo {

    public Ahorro() {
        super();
    }

    public Ahorro(Long id, Double amount, String month, String year) {
        super(id, amount, month, year);
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if(!(obj == null || obj instanceof Ahorro)) {
            return false;
        }
        return ((Ahorro) obj).getDateInfo().equals(this.getDateInfo());
    }

    public int compareTo(Ahorro ahorro){
        return this.getDateInfo().compareTo(ahorro.getDateInfo());
    }
}
