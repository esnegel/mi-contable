package com.snegrete.micontable.model;

import com.snegrete.micontable.enumerator.MonthEnum;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DateInfo implements Comparable<DateInfo> {

    private String month;
    private int monthNumber;
    private String year;

    public DateInfo(String month, String year) {
        this.month = month;
        this.monthNumber = MonthEnum.getMonthNumber(month);
        this.year = year;
    }

    public DateInfo(Integer monthNumber, String year) {
        this.monthNumber = monthNumber;
        this.month = MonthEnum.getMonthName(monthNumber);
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getMonthNumber() {
        return monthNumber;
    }

    public void setMonthNumber(int monthNumber) {
        this.monthNumber = monthNumber;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DateInfo dateInfo = (DateInfo) o;
        return monthNumber == dateInfo.getMonthNumber() &&
                year.equals(dateInfo.year);
    }

    @Override
    public int compareTo(DateInfo dateInfo){
        int yearCmp = Integer.valueOf(this.year).compareTo(Integer.valueOf(dateInfo.getYear()));
        if(yearCmp == 0){
            return Integer.compare(this.monthNumber, dateInfo.monthNumber);
        } else {
            return yearCmp;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(month, year);
    }

    @Override
    public String toString() {
        return month + " " + year;
    }

    public List<DateInfo> generateDatesTo(DateInfo toDate){
        Integer currentYear = Integer.parseInt(this.year);
        Integer toYear = Integer.parseInt(toDate.getYear());
        List<DateInfo> dates = new ArrayList<>();
        for(Integer y = currentYear; y <= toYear; y++){
            for(int m = y > currentYear ? 1 : this.monthNumber; m <= toDate.getMonthNumber(); m++){
                dates.add(new DateInfo(m, y.toString()));
            }
        }
        return dates;
    }
}
