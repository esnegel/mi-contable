package com.snegrete.micontable.model;

import androidx.annotation.Nullable;

public class Gasto extends AmountInfo  {

    private String concepto;

    private String categoria;

    private boolean esFijo;

    public Gasto() {
        super();
    }

    public Gasto(Long id, Double amount, String month, String year, String concepto, String categoria, boolean esFijo) {
        super(id, amount, month, year);
        this.concepto = concepto;
        this.categoria = categoria;
        this.esFijo = esFijo;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public boolean isFijo() {
        return esFijo;
    }

    public void setEsFijo(boolean esFijo) {
        this.esFijo = esFijo;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if(!(obj == null || obj instanceof Gasto)) {
            return false;
        }
        return ((Gasto) obj).getDateInfo().equals(this.getDateInfo());
    }

    public int compareTo(Gasto gasto){
        return this.getDateInfo().compareTo(gasto.getDateInfo());
    }
}
