package com.snegrete.micontable.model;

import androidx.annotation.Nullable;

public class Ingreso extends AmountInfo  {

    private String concepto;

    public Ingreso() {
        super();
    }

    public Ingreso(Long id, Double amount, String month, String year, String concepto) {
        super(id, amount, month, year);
        this.concepto = concepto;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if(!(obj == null || obj instanceof Ingreso)) {
            return false;
        }
        return ((Ingreso) obj).getDateInfo().equals(this.getDateInfo());
    }

    public int compareTo(Ingreso ingreso){
        return this.getDateInfo().compareTo(ingreso.getDateInfo());
    }
}
