package com.snegrete.micontable.model;

public abstract class AmountInfo {

    private Long id;
    private Double amount;
    private DateInfo dateInfo;

    public AmountInfo() {
    }

    public AmountInfo(Long id, Double amount, String month, String year) {
        this.id = id;
        this.amount = amount;
        this.dateInfo = new DateInfo(month, year);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getMonth() {
        return dateInfo.getMonth();
    }

    public void setMonth(String month) {
        dateInfo.setMonth(month);
    }

    public String getYear() {
        return dateInfo.getYear();
    }

    public void setYear(String year) {
        dateInfo.setYear(year);
    }

    public DateInfo getDateInfo() {
        return dateInfo;
    }
}
