package com.snegrete.micontable.database;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class GastoDB extends RealmObject {

    @PrimaryKey
    private Long id;
    private Double amount;
    private String month;
    private String year;
    private String concepto;
    private String categoria;
    private boolean esFijo;

    public GastoDB() {
    }

    public GastoDB(Double amount, String month, String year, String concepto, String categoria, boolean esFijo) {
        this.amount = amount;
        this.month = month;
        this.year = year;
        this.concepto = concepto;
        this.categoria = categoria;
        this.esFijo = esFijo;
    }

    public GastoDB(Long id, Double amount, String month, String year, String concepto, String categoria, boolean esFijo) {
        this.id = id;
        this.amount = amount;
        this.month = month;
        this.year = year;
        this.concepto = concepto;
        this.categoria = categoria;
        this.esFijo = esFijo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public boolean isFijo() {
        return esFijo;
    }

    public void setEsFijo(boolean esFijo) {
        this.esFijo = esFijo;
    }
}
