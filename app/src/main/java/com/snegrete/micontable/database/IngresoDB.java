package com.snegrete.micontable.database;

import androidx.annotation.Nullable;

import com.snegrete.micontable.model.AmountInfo;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class IngresoDB extends RealmObject {

    @PrimaryKey
    private Long id;
    private Double amount;
    private String month;
    private String year;
    private String concepto;

    public IngresoDB() {
    }

    public IngresoDB(Double amount, String month, String year, String concepto, String categoria) {
        this.amount = amount;
        this.month = month;
        this.year = year;
        this.concepto = concepto;
    }

    public IngresoDB(Long id, Double amount, String month, String year, String concepto) {
        this.id = id;
        this.amount = amount;
        this.month = month;
        this.year = year;
        this.concepto = concepto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }
}
