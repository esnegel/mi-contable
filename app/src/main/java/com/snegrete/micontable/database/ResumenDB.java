package com.snegrete.micontable.database;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ResumenDB extends RealmObject {

    @PrimaryKey
    private Long id;
    private Double totalAhorrado;
    private Double objetivo;

    public ResumenDB() {
    }

    public ResumenDB(Double totalAhorrado, Double objetivo) {
        this.totalAhorrado = totalAhorrado;
        this.objetivo = objetivo;
    }

    public ResumenDB(Long id, Double totalAhorrado, Double objetivo) {
        this.id = id;
        this.totalAhorrado = totalAhorrado;
        this.objetivo = objetivo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getTotalAhorrado() {
        return totalAhorrado;
    }

    public void setTotalAhorrado(Double totalAhorrado) {
        this.totalAhorrado = totalAhorrado;
    }

    public Double getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(Double objetivo) {
        this.objetivo = objetivo;
    }
}
