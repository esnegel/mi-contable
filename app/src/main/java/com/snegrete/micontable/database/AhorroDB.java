package com.snegrete.micontable.database;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class AhorroDB extends RealmObject {

    @PrimaryKey
    private Long id;
    private Double amount;
    private String month;
    private String year;

    public AhorroDB() {
    }

    public AhorroDB(Double amount, String month, String year) {
        this.amount = amount;
        this.month = month;
        this.year = year;
    }

    public AhorroDB(Long id, Double amount, String month, String year) {
        this.id = id;
        this.amount = amount;
        this.month = month;
        this.year = year;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
