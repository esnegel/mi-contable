package com.snegrete.micontable.app;

import android.app.Application;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.snegrete.micontable.database.AhorroDB;
import com.snegrete.micontable.database.GastoDB;
import com.snegrete.micontable.database.IngresoDB;
import com.snegrete.micontable.database.ResumenDB;
import com.snegrete.micontable.ui.gastos.recycler.GastoListFragment;
import com.snegrete.micontable.ui.gastos.repository.GastosRepository;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;

public class MiContable extends Application {

    public static AtomicLong ahorroID = new AtomicLong();
    public static AtomicLong gastoID = new AtomicLong();
    public static AtomicLong ingresoID = new AtomicLong();
    public static AtomicLong resumenID = new AtomicLong();

    @Override
    public void onCreate() {
        super.onCreate();
        initRealm();

        Realm realm = Realm.getDefaultInstance();
        ahorroID = getIdByTable(realm, AhorroDB.class);
        gastoID = getIdByTable(realm, GastoDB.class);
        ingresoID = getIdByTable(realm, IngresoDB.class);
        resumenID = getIdByTable(realm, ResumenDB.class);
    }

    private void initRealm() {
        Realm.init(getApplicationContext());
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

    private <T extends RealmObject> AtomicLong getIdByTable(Realm realm, Class<T> anyClass) {
        RealmResults<T> results = realm.where(anyClass).findAll();
        return (results.size() > 0) ? new AtomicLong(results.max("id").intValue()) : new AtomicLong();
    }
}
