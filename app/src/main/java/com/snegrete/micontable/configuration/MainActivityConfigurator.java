package com.snegrete.micontable.configuration;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.snegrete.micontable.R;

public class MainActivityConfigurator {

    private static MainActivityConfigurator mainActivityConfigurator;

    private AppCompatActivity activity;

    private MainActivityConfigurator(){}

    public static MainActivityConfigurator getInstance(AppCompatActivity activity){
        if(mainActivityConfigurator == null){
            mainActivityConfigurator = new MainActivityConfigurator();
            mainActivityConfigurator.activity = activity;
        }
        return mainActivityConfigurator;
    }

    public void configure(){
        activity.setContentView(R.layout.activity_main);
        configureNavigation(getNavController());
    }

    private BottomNavigationView getNavigationView(){
        return activity.findViewById(R.id.nav_view);
    }

    private AppBarConfiguration getAppBarConfig(){
        return new AppBarConfiguration.Builder(
                R.id.navigation_resumen, R.id.navigation_ingresos, R.id.navigation_gastos, R.id.navigation_estadisticas)
                .build();
    }

    private NavController getNavController(){
        return Navigation.findNavController(activity, R.id.nav_host_fragment);
    }

    private void configureNavigation(NavController navController){
        NavigationUI.setupActionBarWithNavController(activity, navController, getAppBarConfig());
        NavigationUI.setupWithNavController(getNavigationView(), navController);
    }
}
