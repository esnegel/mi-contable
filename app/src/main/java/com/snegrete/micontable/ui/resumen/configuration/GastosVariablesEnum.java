package com.snegrete.micontable.ui.resumen.configuration;

import com.snegrete.micontable.R;

import java.util.Arrays;
import java.util.List;

public enum GastosVariablesEnum {

    PRIMERO(R.id.categoria_mes_1, R.id.gasto_mes_1),
    SEGUNDO(R.id.categoria_mes_2, R.id.gasto_mes_2),
    TERCERO(R.id.categoria_mes_3, R.id.gasto_mes_3);

    private int idText;
    private int idProgress;

    GastosVariablesEnum(int idText, int idProgress) {
        this.idText = idText;
        this.idProgress = idProgress;
    }

    public static List<GastosVariablesEnum> getValues(){
        return Arrays.asList(GastosVariablesEnum.values());
    }

    public int getIdText() {
        return idText;
    }

    public int getIdProgress() {
        return idProgress;
    }
}
