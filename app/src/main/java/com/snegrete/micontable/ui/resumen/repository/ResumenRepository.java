package com.snegrete.micontable.ui.resumen.repository;

import com.snegrete.micontable.app.MiContable;
import com.snegrete.micontable.common.repository.RealmRepository;
import com.snegrete.micontable.database.ResumenDB;

import java.util.List;

import io.realm.Realm;

public class ResumenRepository extends RealmRepository<ResumenDB> {

    private static ResumenRepository resumenRepository = new ResumenRepository();

    public static ResumenRepository getInstance() {
        return resumenRepository;
    }

    private ResumenRepository() {
        super(ResumenDB.class);
    }

    public ResumenDB findResumen(){
        List<ResumenDB> resumenes = super.findAll();
        if(resumenes.isEmpty()){
            return initResumen();
        }
        return resumenes.get(0);
    }

    private ResumenDB initResumen(){
        ResumenDB resumenDB = new ResumenDB(MiContable.resumenID.incrementAndGet(), 0D, 0D);
        super.save(resumenDB);
        return resumenDB;
    }

    public void saveObjetivo(ResumenDB resumen, Double objetivo){
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                resumen.setObjetivo(objetivo);
                realm.copyToRealmOrUpdate(resumen);
            }
        });
        realm.close();
    }

    public void saveAhorrado(ResumenDB resumen, Double ahorrado){
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                resumen.setTotalAhorrado(ahorrado);
                realm.copyToRealmOrUpdate(resumen);
            }
        });
        realm.close();
    }
}
