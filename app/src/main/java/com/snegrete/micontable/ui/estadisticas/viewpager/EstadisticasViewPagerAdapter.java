package com.snegrete.micontable.ui.estadisticas.viewpager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.snegrete.micontable.ui.estadisticas.charts.common.ChartFragment;
import com.snegrete.micontable.ui.estadisticas.charts.gastos.categoria.GastosCategoriaChartFragment;
import com.snegrete.micontable.ui.estadisticas.charts.resumen.ResumenChartFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EstadisticasViewPagerAdapter extends FragmentPagerAdapter {

    List<Class<? extends ChartFragment>> charts = new ArrayList<>();

    public EstadisticasViewPagerAdapter(@NonNull FragmentManager fm) {
        super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        init();
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        //position = position % charts.size();
        try {
            return charts.get(position).newInstance();
        } catch (IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int getCount() {
        return charts.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return "Chart";
    }

    public void init() {
        charts.addAll(Arrays.asList(
                GastosCategoriaChartFragment.class,
                ResumenChartFragment.class));
    }
}
