package com.snegrete.micontable.ui.resumen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.snegrete.micontable.R;
import com.snegrete.micontable.ui.gastos.service.GastoService;
import com.snegrete.micontable.ui.ingresos.service.IngresoService;
import com.snegrete.micontable.ui.resumen.ahorros.service.AhorroService;
import com.snegrete.micontable.ui.resumen.configuration.ResumenFragmentConfigurator;
import com.snegrete.micontable.ui.resumen.service.ResumenService;
import com.snegrete.micontable.ui.resumen.service.dto.GastoVariableInfoDTO;
import com.snegrete.micontable.utilities.CalendarUtils;

import java.util.List;

public class ResumenFragment extends Fragment {

    private ResumenFragmentConfigurator resumenFragmentConfigurator = ResumenFragmentConfigurator.getInstance();

    private Integer totalAhorrado;
    private Integer ahorroInicial;
    private Integer objetivoAhorro;
    private Double ahorroMedio;
    private Integer mesesRestantes;
    private Double mesIngresos;
    private Double mesGastos;
    private Double mesGastosVariables;
    private List<GastoVariableInfoDTO> gastosVariablesTop;

    private View view;

    @Nullable
    @Override
    public View getView() {
        return view;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_resumen, container, false);
        update();
        return view;
    }

    public void update() {
        ResumenService resumenService = ResumenService.getInstance();
        AhorroService ahorroService = AhorroService.getInstance();
        IngresoService ingresoService = IngresoService.getInstance();
        GastoService gastoService = GastoService.getInstance();

        // Resumen General
        ahorroMedio = ahorroService.calculaAhorroMedio().orElse(0D);
        objetivoAhorro = resumenService.getObjetivo().intValue();
        totalAhorrado = resumenService.getTotalAhorrado().intValue();
        ahorroInicial = resumenService.getAhorroInicial().intValue();
        mesesRestantes = resumenService.calculaMesesRestantes(ahorroMedio);

        // Resumen mes Actual
        mesIngresos = ingresoService.getTotalIngresos(CalendarUtils.getActualDateInfo());
        mesGastos = gastoService.getTotalGastos(CalendarUtils.getActualDateInfo());
        mesGastosVariables = gastoService.getGastosVariables(CalendarUtils.getActualDateInfo());
        gastosVariablesTop = gastoService.getTopGastosVariables(CalendarUtils.getActualDateInfo());

        resumenFragmentConfigurator.configure(this);
    }

    public void updateCardViews(){
        ResumenService resumenService = ResumenService.getInstance();
        objetivoAhorro = resumenService.getObjetivo().intValue();
        totalAhorrado = resumenService.getTotalAhorrado().intValue();
        ahorroInicial = resumenService.getAhorroInicial().intValue();
        resumenFragmentConfigurator.configureGlobalResumen(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public Integer getTotalAhorrado() {
        return totalAhorrado;
    }

    public Integer getAhorroInicial() {
        return ahorroInicial;
    }

    public Integer getObjetivoAhorro() {
        return objetivoAhorro;
    }

    public Double getAhorroMedio() {
        return ahorroMedio;
    }

    public Integer getMesesRestantes() {
        return mesesRestantes;
    }

    public Double getMesIngresos() {
        return mesIngresos;
    }

    public List<GastoVariableInfoDTO> getGastosVariablesTop() {
        return gastosVariablesTop;
    }

    public Double getMesGastos() {
        return mesGastos;
    }

    public Double getCapacidadAhorro() {
        return mesIngresos - mesGastos;
    }

    public Double getMesGastosVariables() {
        return mesGastosVariables;
    }
}