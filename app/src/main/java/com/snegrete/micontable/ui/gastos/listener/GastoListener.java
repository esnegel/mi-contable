package com.snegrete.micontable.ui.gastos.listener;

import android.view.View;

import com.snegrete.micontable.model.Gasto;

public interface GastoListener {

    void addGasto(Gasto gasto);
    void editGasto(Gasto gasto);
    void deleteGasto(Gasto gasto);

    View.OnLongClickListener onClickGasto(Gasto gasto);
}
