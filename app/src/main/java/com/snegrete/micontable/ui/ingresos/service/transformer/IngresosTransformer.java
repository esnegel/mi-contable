package com.snegrete.micontable.ui.ingresos.service.transformer;

import com.snegrete.micontable.database.IngresoDB;
import com.snegrete.micontable.model.Ingreso;

public class IngresosTransformer {

    private static IngresosTransformer ingresosTransformer = new IngresosTransformer();

    private IngresosTransformer() {
    }

    public static IngresosTransformer getInstance() {
        if (ingresosTransformer == null) {
            ingresosTransformer = new IngresosTransformer();
        }
        return ingresosTransformer;
    }

    public IngresoDB toIngresoDB(Ingreso ingreso) {
        return new IngresoDB(
                ingreso.getId(),
                ingreso.getAmount(),
                ingreso.getMonth(),
                ingreso.getYear(),
                ingreso.getConcepto()
        );
    }

    public Ingreso toIngreso(IngresoDB ingresoDB) {
        return new Ingreso(
                ingresoDB.getId(),
                ingresoDB.getAmount(),
                ingresoDB.getMonth(),
                ingresoDB.getYear(),
                ingresoDB.getConcepto()
        );
    }
}
