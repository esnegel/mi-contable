package com.snegrete.micontable.ui.resumen.ahorros.recycler;

import android.content.DialogInterface;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.snegrete.micontable.R;
import com.snegrete.micontable.model.Ahorro;
import com.snegrete.micontable.ui.resumen.ahorros.AhorroFragment;
import com.snegrete.micontable.ui.resumen.ahorros.dialog.AhorroDialog;
import com.snegrete.micontable.ui.resumen.ahorros.listener.AhorroListener;
import com.snegrete.micontable.utilities.CalendarUtils;
import com.snegrete.micontable.utilities.MoneyUtils;
import com.snegrete.micontable.utilities.ToastUtils;

public class AhorroViewHolder extends RecyclerView.ViewHolder {

    private View view;

    private TextView amount;
    private TextView month;
    private TextView year;

    public AhorroViewHolder(View view) {
        super(view);
        this.view = view;
        this.amount = view.findViewById(R.id.fecha_fin_ahorro);
        this.month = view.findViewById(R.id.ahorro_month);
        this.year = view.findViewById(R.id.ahorro_year);
    }

    public void bind(Ahorro ahorro, AhorroListener ahorroListener){
        month.setText(ahorro.getMonth());
        year.setText(ahorro.getYear());
        amount.setText(MoneyUtils.formatAmount(ahorro.getAmount()));

        view.setOnLongClickListener(ahorroListener.onClickAhorro(ahorro));
    }
}
