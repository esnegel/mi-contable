package com.snegrete.micontable.ui.resumen.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.snegrete.micontable.R;
import com.snegrete.micontable.ui.resumen.listener.ResumenListener;

public class ObjetivoDialog extends DialogFragment {

    private ResumenListener resumenListener;

    private String objetivo;

    public ObjetivoDialog(String objetivo) {
        this.objetivo = objetivo;
    }

    public String getAhorro() {
        return objetivo;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        View dialogLayout = getActivity().getLayoutInflater().inflate(R.layout.dialog_objetivo, null);

        AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setView(dialogLayout)
                .setCancelable(false)
                .setPositiveButton(R.string.btn_aceptar, getNewButton())
                .setNegativeButton(R.string.btn_cancelar, getCancelButton())
                .create();

        ((EditText)dialogLayout.findViewById(R.id.objetivoEditText)).setText(objetivo);
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

    private DialogInterface.OnClickListener getNewButton() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                resumenListener.updateObjetivo(((EditText)((AlertDialog) dialog).findViewById(R.id.objetivoEditText)).getEditableText().toString());
                dialog.dismiss();
            }
        };
    }

    private DialogInterface.OnClickListener getCancelButton() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        };
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        resumenListener = (ResumenListener) context;
    }
}
