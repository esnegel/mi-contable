package com.snegrete.micontable.ui.ingresos.recycler;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.snegrete.micontable.R;
import com.snegrete.micontable.model.Ingreso;
import com.snegrete.micontable.ui.ingresos.listener.IngresoListener;

import java.util.List;

public class IngresoListRecyclerViewAdapter extends RecyclerView.Adapter<IngresoViewHolder> {

    private final List<Ingreso> ingresos;
    private final IngresoListener ingresoListener;

    public IngresoListRecyclerViewAdapter(List<Ingreso> ingresos, IngresoListener listener) {
        this.ingresos = ingresos;
        ingresoListener = listener;
    }

    @Override
    public IngresoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_ingreso, parent, false);
        return new IngresoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final IngresoViewHolder holder, int position) {
        holder.bind(ingresos.get(position), ingresoListener);
    }

    @Override
    public int getItemCount() {
        return ingresos.size();
    }
}
