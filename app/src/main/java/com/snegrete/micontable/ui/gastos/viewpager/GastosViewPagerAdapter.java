package com.snegrete.micontable.ui.gastos.viewpager;

import android.util.Log;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.snegrete.micontable.common.TabAmount;
import com.snegrete.micontable.model.DateInfo;
import com.snegrete.micontable.model.Gasto;
import com.snegrete.micontable.ui.gastos.listener.GastoListener;
import com.snegrete.micontable.ui.gastos.recycler.GastoListFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GastosViewPagerAdapter extends FragmentPagerAdapter {

    private GastoListener gastoListener;

    private List<TabAmount<DateInfo, Gasto>> tabAmountList;

    public GastosViewPagerAdapter(@NonNull FragmentManager fm, List<TabAmount<DateInfo, Gasto>> tabAmountList, GastoListener gastoListener) {
        super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.gastoListener = gastoListener;
        this.tabAmountList = tabAmountList;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Log.i("Gastos", "Position: " + position);
        return GastoListFragment.newInstance(tabAmountList.get(position).getAmountInfoList(), tabAmountList.get(position).getDateInfo(), gastoListener);
    }

    @Override
    public int getCount() {
        return tabAmountList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return (String) tabAmountList.get(position).getDateInfo().toString();
    }

    public void setTabAmountList(List<TabAmount<DateInfo, Gasto>> tabAmountList) {
        this.tabAmountList = tabAmountList;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        ((GastoListFragment) object).update();
        return super.getItemPosition(object);
    }
}
