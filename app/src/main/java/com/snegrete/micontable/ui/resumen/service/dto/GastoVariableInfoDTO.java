package com.snegrete.micontable.ui.resumen.service.dto;

import androidx.annotation.NonNull;

import com.snegrete.micontable.utilities.MoneyUtils;

public class GastoVariableInfoDTO implements Comparable<GastoVariableInfoDTO> {

    private String categoria;
    private Double cantidad;

    public GastoVariableInfoDTO(String categoria, Double cantidad) {
        this.categoria = categoria;
        this.cantidad = cantidad;
    }

    public Double getCantidad() {
        return cantidad;
    }

    @Override
    public int compareTo(GastoVariableInfoDTO o) {
        return o.getCantidad().compareTo(this.cantidad);
    }

    @NonNull
    @Override
    public String toString() {
        return categoria + " ("+ MoneyUtils.formatAmount(cantidad) +")";
    }
}
