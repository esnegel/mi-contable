package com.snegrete.micontable.ui.estadisticas.charts.common.drawer;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;

import com.github.mikephil.charting.charts.Chart;
import com.snegrete.micontable.R;
import com.snegrete.micontable.ui.estadisticas.charts.common.ChartFragment;

public abstract class ChartDrawer {

    private ChartFragment fragment;

    protected int primaryColor;
    protected int accentColor;

    public ChartDrawer(ChartFragment fragment){
        this.fragment = fragment;
        primaryColor = getColor(R.color.colorPrimaryDark);
        accentColor = getColor(R.color.colorAccent);
    }

    public ChartFragment getFragment() {
        return fragment;
    }

    public View getView(){
        return fragment.getView();
    }

    public View getView(int id){
        return getView().findViewById(id);
    }

    public Context getContext(){
        return fragment.getContext();
    }

    public int getColor(int id){
        return getResources().getColor(id, null);
    }

    public String[] getStringArray(int id){
        return getResources().getStringArray(id);
    }

    private Resources getResources(){
        return fragment.getResources();
    }

    public abstract void configure();
}
