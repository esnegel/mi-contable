package com.snegrete.micontable.ui.gastos.recycler;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.snegrete.micontable.R;
import com.snegrete.micontable.model.DateInfo;
import com.snegrete.micontable.model.Gasto;
import com.snegrete.micontable.ui.gastos.listener.GastoListener;
import com.snegrete.micontable.ui.gastos.service.GastoService;

import java.util.List;

public class GastoListFragment extends Fragment {

    private GastoService gastoService = GastoService.getInstance();

    private GastoListener gastoListener;
    private List<Gasto> gastos;
    private View view;
    private DateInfo dateInfo;

    public static GastoListFragment newInstance(List<Gasto> gastos, DateInfo dateInfo, GastoListener gastoListener) {
        Log.i("Gastos", "New Instance");
        GastoListFragment gastoListFragment = new GastoListFragment();
        gastoListFragment.gastos = gastos;
        gastoListFragment.gastoListener = gastoListener;
        gastoListFragment.dateInfo = dateInfo;

        Bundle bundle = new Bundle();
        bundle.putString("title", dateInfo.toString());
        gastoListFragment.setArguments(bundle);

        return gastoListFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_list_gasto, container, false);

        Log.i("Gastos", "Create List Fragment");

        RecyclerView lista = view.findViewById(R.id.gasto_list);
        lista.setLayoutManager(new LinearLayoutManager(lista.getContext()));
        lista.setAdapter(new GastoListRecyclerViewAdapter(gastos, gastoListener));

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(lista.getContext(), LinearLayoutManager.VERTICAL);
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.divider_vertical));
        lista.addItemDecoration(dividerItemDecoration);

        return view;
    }

    public void update(){
        if(view != null){
            RecyclerView lista = view.findViewById(R.id.gasto_list);
            gastos = gastoService.getGastosPorFecha(dateInfo);
            lista.setAdapter(new GastoListRecyclerViewAdapter(gastos, gastoListener));
        }
    }

    public List<Gasto> getGastos() {
        return gastos;
    }

    public DateInfo getDateInfo() {
        return dateInfo;
    }
}
