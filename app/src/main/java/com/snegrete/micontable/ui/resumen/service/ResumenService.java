package com.snegrete.micontable.ui.resumen.service;

import com.snegrete.micontable.database.ResumenDB;
import com.snegrete.micontable.ui.resumen.ahorros.service.AhorroService;
import com.snegrete.micontable.ui.resumen.repository.ResumenRepository;

public class ResumenService {

    private static ResumenService resumenService = new ResumenService();

    private ResumenService(){
    }

    public static ResumenService getInstance(){
        if(resumenService == null){
            resumenService = new ResumenService();
        }
        return resumenService;
    }

    private AhorroService ahorroService = AhorroService.getInstance();

    private ResumenRepository resumenRepository = ResumenRepository.getInstance();

    public void saveObjetivo(Double objetivo) {
        ResumenDB resumen = resumenRepository.findResumen();
        resumenRepository.saveObjetivo(resumen, objetivo);
    }

    public void saveAhorrado(Double ahorrado) {
        ResumenDB resumen = resumenRepository.findResumen();
        resumenRepository.saveAhorrado(resumen, ahorrado);
    }

    public Double getObjetivo(){
        return resumenRepository.findResumen().getObjetivo();
    }

    public Double getTotalAhorrado(){
        return Double.sum(getAhorroInicial(), ahorroService.getTotalAhorros());
    }

    public Integer calculaMesesRestantes(Double ahorroMedio) {
        Double restante = getObjetivo() - getTotalAhorrado();
        Double mesesRestantes = restante / ahorroMedio;
        return (int) Math.round(mesesRestantes);
    }

    public Double getAhorroInicial() {
        return resumenRepository.findResumen().getTotalAhorrado();
    }
}
