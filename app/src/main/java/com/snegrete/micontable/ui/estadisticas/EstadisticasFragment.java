package com.snegrete.micontable.ui.estadisticas;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.snegrete.micontable.R;
import com.snegrete.micontable.ui.estadisticas.viewpager.EstadisticasViewPagerAdapter;
import com.snegrete.micontable.ui.gastos.service.GastoService;
import com.snegrete.micontable.ui.gastos.viewpager.GastosViewPagerAdapter;

public class EstadisticasFragment extends Fragment {

    private View view;

    private GastoService gastoService = GastoService.getInstance();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_estadisticas, container, false);
        ViewPager viewPager = getView().findViewById(R.id.ChartsViewPager);
        viewPager.setAdapter(new EstadisticasViewPagerAdapter(getChildFragmentManager()));
        //viewPager.setCurrentItem(Integer.MAX_VALUE / 2, false);
        return view;
    }

    @Nullable
    @Override
    public View getView() {
        return view;
    }
}
