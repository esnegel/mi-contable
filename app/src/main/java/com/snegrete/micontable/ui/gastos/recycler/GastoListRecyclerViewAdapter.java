package com.snegrete.micontable.ui.gastos.recycler;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.snegrete.micontable.R;
import com.snegrete.micontable.model.Gasto;
import com.snegrete.micontable.model.Ingreso;
import com.snegrete.micontable.ui.gastos.listener.GastoListener;

import java.util.List;

public class GastoListRecyclerViewAdapter extends RecyclerView.Adapter<GastoViewHolder> {

    private final List<Gasto> gastos;
    private final GastoListener gastoListener;

    public GastoListRecyclerViewAdapter(List<Gasto> gastos, GastoListener listener) {
        this.gastos = gastos;
        gastoListener = listener;
    }

    @Override
    public GastoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_gasto, parent, false);
        return new GastoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final GastoViewHolder holder, int position) {
        holder.bind(gastos.get(position), gastoListener);
    }

    @Override
    public int getItemCount() {
        return gastos.size();
    }
}
