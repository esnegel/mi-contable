package com.snegrete.micontable.ui.resumen.ahorros.recycler;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.snegrete.micontable.R;
import com.snegrete.micontable.model.Ahorro;
import com.snegrete.micontable.ui.resumen.ahorros.listener.AhorroListener;

import java.util.List;

public class AhorroRecyclerViewAdapter extends RecyclerView.Adapter<AhorroViewHolder> {

    private List<Ahorro> ahorros;

    private final AhorroListener ahorroListener;

    public AhorroRecyclerViewAdapter(List<Ahorro> ahorros, AhorroListener listener) {
        this.ahorros = ahorros;
        ahorroListener = listener;
    }

    @Override
    public AhorroViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_ahorro, parent, false);
        return new AhorroViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AhorroViewHolder holder, int position) {
        holder.bind(ahorros.get(position), ahorroListener);
    }

    @Override
    public int getItemCount() {
        return ahorros.size();
    }

}
