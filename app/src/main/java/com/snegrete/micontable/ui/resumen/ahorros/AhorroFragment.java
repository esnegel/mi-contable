package com.snegrete.micontable.ui.resumen.ahorros;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.snegrete.micontable.R;
import com.snegrete.micontable.model.Ahorro;
import com.snegrete.micontable.ui.resumen.ResumenFragment;
import com.snegrete.micontable.ui.resumen.ahorros.configuration.AhorroFragmentConfigurator;
import com.snegrete.micontable.ui.resumen.ahorros.dialog.AhorroDialog;
import com.snegrete.micontable.ui.resumen.ahorros.listener.AhorroListener;
import com.snegrete.micontable.ui.resumen.ahorros.recycler.AhorroRecyclerViewAdapter;
import com.snegrete.micontable.ui.resumen.ahorros.service.AhorroService;

import java.util.List;

public class AhorroFragment extends Fragment {

    private AhorroListener ahorroListener;

    private AhorroFragmentConfigurator ahorroFragmentConfigurator = AhorroFragmentConfigurator.getInstance();

    private View view;

    @Nullable
    @Override
    public View getView() {
        return view;
    }

    public AhorroListener getAhorroListener() {
        return ahorroListener;
    }

    private List<Ahorro> ahorros;

    public List<Ahorro> getAhorros() {
        return ahorros;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_ahorro, container, false);

        AhorroService ahorroService = AhorroService.getInstance();
        ahorros = ahorroService.getAhorros();
        ahorroFragmentConfigurator.configure(this);

        return view;
    }

    public void onDismissAhorroDialog(DialogInterface dialog){
        AhorroService ahorroService = AhorroService.getInstance();
        ahorros = ahorroService.getAhorros();
        ahorroFragmentConfigurator.updateRecyclerView();
        ((ResumenFragment)this.getParentFragment()).update();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ahorroListener = (AhorroListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ahorroListener = null;
    }
}
