package com.snegrete.micontable.ui.resumen.ahorros.configuration;

import android.content.DialogInterface;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.snegrete.micontable.R;
import com.snegrete.micontable.ui.resumen.ahorros.AhorroFragment;
import com.snegrete.micontable.ui.resumen.ahorros.dialog.AhorroDialog;
import com.snegrete.micontable.ui.resumen.ahorros.recycler.AhorroRecyclerViewAdapter;

public class AhorroFragmentConfigurator {

    private static AhorroFragmentConfigurator ahorroFragmentConfigurator = new AhorroFragmentConfigurator();
    private AhorroFragment fragment;

    private AhorroFragmentConfigurator() {
    }

    public static AhorroFragmentConfigurator getInstance() {
        if (ahorroFragmentConfigurator == null) {
            ahorroFragmentConfigurator = new AhorroFragmentConfigurator();
        }
        return ahorroFragmentConfigurator;
    }

    public void configure(AhorroFragment fragment){
        this.fragment = fragment;
        configureRecyclerView();
        configureAddAhorroButton();
    }

    public void configure(){
        if(fragment!= null) {
            configureRecyclerView();
            configureAddAhorroButton();
        }
    }

    public void configureRecyclerView(){
        RecyclerView recyclerView = (RecyclerView) fragment.getView().findViewById(R.id.ahorro_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(fragment.getContext(), LinearLayoutManager.HORIZONTAL, true));
        recyclerView.setAdapter(new AhorroRecyclerViewAdapter(fragment.getAhorros(),
                fragment.getAhorroListener()));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.HORIZONTAL);
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(fragment.getActivity(), R.drawable.divider_horizontal_primary));
        recyclerView.addItemDecoration(dividerItemDecoration);

    }

    public void updateRecyclerView(){
        RecyclerView recyclerView = fragment.getView().findViewById(R.id.ahorro_list);
        recyclerView.setAdapter(new AhorroRecyclerViewAdapter(fragment.getAhorros(),
                fragment.getAhorroListener()));
    }

    public void configureAddAhorroButton(){
        ImageButton button = fragment.getView().findViewById(R.id.ahorro_add_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAhorroDialog();
            }
        });
    }

    private void showAhorroDialog(){
        AhorroDialog ahorroDialog = new AhorroDialog();
        ahorroDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                fragment.onDismissAhorroDialog(dialog);
            }
        });
        ahorroDialog.show(fragment.getFragmentManager(), "Nuevo AhorroDB");
    }
}
