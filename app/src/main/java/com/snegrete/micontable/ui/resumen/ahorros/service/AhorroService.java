package com.snegrete.micontable.ui.resumen.ahorros.service;

import com.snegrete.micontable.database.AhorroDB;
import com.snegrete.micontable.model.Ahorro;
import com.snegrete.micontable.ui.resumen.ahorros.repository.AhorrosRepository;
import com.snegrete.micontable.ui.resumen.ahorros.service.transformer.AhorrosTransformer;
import com.snegrete.micontable.utilities.ReverseStream;

import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

public class AhorroService {

    private static AhorroService ahorroService = new AhorroService();

    private AhorroService(){
    }

    public static AhorroService getInstance(){
        if(ahorroService == null){
            ahorroService = new AhorroService();
        }
        return ahorroService;
    }

    private AhorrosRepository ahorrosRepository = AhorrosRepository.getInstance();
    private AhorrosTransformer ahorrosTransformer = AhorrosTransformer.getInstance();

    public List<Ahorro> getAhorros() {
        return ahorrosRepository.findAll().stream()
                .map(db -> ahorrosTransformer.toAhorro(db))
                .sorted(Ahorro::compareTo)
                .collect(ReverseStream.reverse())
                .collect(Collectors.toList());
    }

    public Ahorro findAhorro(Ahorro ahorro){
        AhorroDB ahorroDB = ahorrosRepository.findById(ahorro.getId());
        return ahorroDB == null ? null : ahorrosTransformer.toAhorro(ahorroDB);
    }

    public OptionalDouble calculaAhorroMedio(){
        return getAhorros()
                .stream()
                .filter(a -> !a.getAmount().equals(Double.NaN))
                .mapToDouble(Ahorro::getAmount)
                .average();
    }

    public Double getTotalAhorros(){
        return getAhorros().stream().mapToDouble(Ahorro::getAmount).sum();
    }

    public void save(Ahorro ahorro){
        ahorrosRepository.save(ahorrosTransformer.toAhorroDB(ahorro));
    }

    public void delete(Ahorro ahorro) {
        ahorrosRepository.delete(ahorro.getId());
    }
}
