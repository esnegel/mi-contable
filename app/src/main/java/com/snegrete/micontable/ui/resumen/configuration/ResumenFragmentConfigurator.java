package com.snegrete.micontable.ui.resumen.configuration;

import android.view.View;

import androidx.core.content.res.ResourcesCompat;

import com.snegrete.micontable.R;
import com.snegrete.micontable.common.configurationbean.progressbar.ProgressBarConfiguratorBean;
import com.snegrete.micontable.common.configurationbean.progressbar.ProgressBarInfoDTO;
import com.snegrete.micontable.common.configurationbean.textview.TextViewConfiguratorBean;
import com.snegrete.micontable.common.configurationbean.textview.TextViewInfoDTO;
import com.snegrete.micontable.ui.resumen.ResumenFragment;
import com.snegrete.micontable.ui.resumen.dialog.AhorradoDialog;
import com.snegrete.micontable.ui.resumen.dialog.ObjetivoDialog;
import com.snegrete.micontable.ui.resumen.service.dto.GastoVariableInfoDTO;
import com.snegrete.micontable.utilities.CalendarUtils;
import com.snegrete.micontable.utilities.MoneyUtils;

import java.util.Arrays;
import java.util.List;

public class ResumenFragmentConfigurator {

    private static ResumenFragmentConfigurator resumenFragmentConfigurator = new ResumenFragmentConfigurator();

    private ResumenFragment fragment;

    private TextViewConfiguratorBean textViewConfiguratorBean;

    private ResumenFragmentConfigurator() {
    }

    public static ResumenFragmentConfigurator getInstance() {
        if (resumenFragmentConfigurator == null) {
            resumenFragmentConfigurator = new ResumenFragmentConfigurator();
        }
        return resumenFragmentConfigurator;
    }

    public void configure(ResumenFragment fragment) {
        this.fragment = fragment;

        textViewConfiguratorBean = new TextViewConfiguratorBean(fragment.getView());

        configureTextViews();
        configureAhorroProgress();

        configureGastosMesProgress();

        configureCardViewObjetivo();
        configureCardViewAhorrado();
    }

    public void configureGlobalResumen(ResumenFragment fragment){
        this.fragment = fragment;
        textViewConfiguratorBean = new TextViewConfiguratorBean(fragment.getView());
        configureCardViewObjetivo();
        configureCardViewAhorrado();
        configureAhorroProgress();
    }

    private void configureTextViews() {
        textViewConfiguratorBean.configureAll(
                Arrays.asList(
                        new TextViewInfoDTO(R.id.ahorroActual, MoneyUtils.formatAmount(fragment.getTotalAhorrado())),
                        new TextViewInfoDTO(R.id.ahorroMedio, MoneyUtils.formatAmount(fragment.getAhorroMedio())),
                        new TextViewInfoDTO(R.id.fecha_fin_ahorro,
                                CalendarUtils.addMonthsToActualDate(fragment.getMesesRestantes(), fragment.getContext())),
                        new TextViewInfoDTO(R.id.totalIngresos, MoneyUtils.formatAmount(fragment.getMesIngresos())),
                        new TextViewInfoDTO(R.id.totalGastos, MoneyUtils.formatAmount(fragment.getMesGastos())),
                        new TextViewInfoDTO(R.id.capacidadAhorro, MoneyUtils.formatAmount(fragment.getCapacidadAhorro())),
                        new TextViewInfoDTO(R.id.gastosVariables, MoneyUtils.formatAmount(fragment.getMesGastosVariables()))));
    }

    private void configureAhorroProgress() {
        new ProgressBarConfiguratorBean(
                fragment,
                new ProgressBarInfoDTO(
                        R.id.progresoAhorro,
                        Math.round(fragment.getObjetivoAhorro()),
                        Math.round(fragment.getTotalAhorrado()),
                        new TextViewInfoDTO(R.id.mesesRestantes,
                                String.format(
                                        fragment.getString(R.string.progreso_ahorro),
                                        MoneyUtils.formatAmount(fragment.getObjetivoAhorro() - fragment.getTotalAhorrado())))))
                .configure();
    }

    private void configureGastosMesProgress() {
        List<GastosVariablesEnum> gastosVariablesViews = GastosVariablesEnum.getValues();
        List<GastoVariableInfoDTO> gastosVariables = fragment.getGastosVariablesTop();

        for (int i = 0; i < gastosVariablesViews.size(); i++) {
            ProgressBarInfoDTO progressBarInfoDTO;
            if (i < gastosVariables.size()) {
                progressBarInfoDTO = new ProgressBarInfoDTO(
                        gastosVariablesViews.get(i).getIdProgress(),
                        Long.valueOf(Math.round(fragment.getMesGastosVariables())).intValue(),
                        Long.valueOf(Math.round(gastosVariables.get(i).getCantidad())).intValue(),
                        new TextViewInfoDTO(
                                gastosVariablesViews.get(i).getIdText(),
                                gastosVariables.get(i).toString()));
            } else {
                progressBarInfoDTO = new ProgressBarInfoDTO(
                        new TextViewInfoDTO(gastosVariablesViews.get(i).getIdText()),
                        gastosVariablesViews.get(i).getIdProgress());
            }
            ProgressBarConfiguratorBean progressBarConfiguratorBean = new ProgressBarConfiguratorBean(fragment, progressBarInfoDTO);
            progressBarConfiguratorBean.setBackground(ResourcesCompat.getDrawable(fragment.getResources(), android.R.color.white, null));
            progressBarConfiguratorBean.configure();
        }
    }

    private void configureCardViewObjetivo() {
        fragment.getView().findViewById(R.id.cardViewObjetivo).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                new ObjetivoDialog(fragment.getObjetivoAhorro().toString()).show(fragment.getFragmentManager(), "Editar Objetivo");
                return true;
            }
        });
        textViewConfiguratorBean.configure(new TextViewInfoDTO(R.id.ahorroObjetivo, MoneyUtils.formatAmount(fragment.getObjetivoAhorro())));
    }

    private void configureCardViewAhorrado() {
        fragment.getView().findViewById(R.id.cardViewAhorrado).setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                new AhorradoDialog(fragment.getAhorroInicial().toString()).show(fragment.getFragmentManager(), "Editar Ahorrado");
                return true;
            }
        });
        textViewConfiguratorBean.configure(new TextViewInfoDTO(R.id.ahorroActual, MoneyUtils.formatAmount(fragment.getTotalAhorrado())));
    }
}
