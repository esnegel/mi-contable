package com.snegrete.micontable.ui.gastos.dialog;


import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;

import androidx.appcompat.app.AlertDialog;

import com.snegrete.micontable.R;
import com.snegrete.micontable.model.Gasto;

public class GastoDialogTransformer {

    private static GastoDialogTransformer gastoDialogTransformer = new GastoDialogTransformer();

    private GastoDialogTransformer(){
    }

    public static GastoDialogTransformer getInstance(){
        if(gastoDialogTransformer == null){
            gastoDialogTransformer = new GastoDialogTransformer();
        }
        return gastoDialogTransformer;
    }

    private static final int ID_CANTIDAD = R.id.addGastoCantidad;
    private static final int ID_MONTH = R.id.addGastoMonth;
    private static final int ID_YEAR = R.id.addGastoYear;
    private static final int ID_CONCEPTO = R.id.addGastoConcepto;
    private static final int ID_CATEGORIA = R.id.addGastoCategoria;
    private static final int ID_GASTO_FIJO = R.id.addGastoEsFijo;
    private static final int ID_ID = R.id.addGastoId;

    public Gasto toGasto(AlertDialog gastoDialog){
        String cantidad = getTextByID(gastoDialog, ID_CANTIDAD);
        String concepto = getTextByID(gastoDialog, ID_CONCEPTO);
        String id = getTextByID(gastoDialog, ID_ID);
        String month = getSpinnerTextByID(gastoDialog, ID_MONTH);
        String year = getSpinnerTextByID(gastoDialog, ID_YEAR);
        String categoria = getSpinnerTextByID(gastoDialog, ID_CATEGORIA);
        boolean esGastoFijo = ((Switch)gastoDialog.findViewById(ID_GASTO_FIJO)).isChecked();

        return new Gasto(
                "".equals(id) ? null : Long.valueOf(id),
                "".equals(cantidad) ? Double.NaN : Double.valueOf(cantidad),
                month,
                year,
                concepto,
                categoria,
                esGastoFijo);
    }

    private String getTextByID(AlertDialog dialog, int id){
        return ((EditText)(dialog.findViewById(id))).getEditableText().toString();
    }

    private String getSpinnerTextByID(AlertDialog dialog, int id){
        return ((Spinner)(dialog.findViewById(id))).getSelectedItem().toString();
    }
}
