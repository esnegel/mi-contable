package com.snegrete.micontable.ui.ingresos.recycler;

import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.snegrete.micontable.R;
import com.snegrete.micontable.model.DateInfo;
import com.snegrete.micontable.model.Ingreso;
import com.snegrete.micontable.ui.ingresos.listener.IngresoListener;
import com.snegrete.micontable.ui.ingresos.service.IngresoService;

import java.util.List;

public class IngresoListFragment extends Fragment {

    private IngresoService ingresoService = IngresoService.getInstance();

    private IngresoListener ingresoListener;
    private List<Ingreso> ingresos;
    private View view;
    private DateInfo dateInfo;

    public static IngresoListFragment newInstance(List<Ingreso> ingresos, DateInfo dateInfo, IngresoListener ingresoListener) {
        IngresoListFragment ingresoListFragment = new IngresoListFragment();
        ingresoListFragment.ingresos = ingresos;
        ingresoListFragment.ingresoListener = ingresoListener;
        ingresoListFragment.dateInfo = dateInfo;

        Bundle bundle = new Bundle();
        bundle.putString("title", dateInfo.toString());
        ingresoListFragment.setArguments(bundle);

        return ingresoListFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i("ListIngreso", "Ingresos " + dateInfo.toString());
        view = inflater.inflate(R.layout.fragment_list_ingreso, container, false);

        RecyclerView lista = view.findViewById(R.id.ingreso_list);
        lista.setLayoutManager(new LinearLayoutManager(lista.getContext()));
        lista.setAdapter(new IngresoListRecyclerViewAdapter(ingresos, ingresoListener));

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(lista.getContext(), LinearLayoutManager.VERTICAL);
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.divider_vertical));
        lista.addItemDecoration(dividerItemDecoration);

        return view;
    }

    public void update(){
        if(view != null){
            RecyclerView lista = view.findViewById(R.id.ingreso_list);
            ingresos = ingresoService.getIngresos(dateInfo);
            lista.setAdapter(new IngresoListRecyclerViewAdapter(ingresos, ingresoListener));
        }
    }

    public List<Ingreso> getIngresos() {
        return ingresos;
    }

    public DateInfo getDateInfo() {
        return dateInfo;
    }
}
