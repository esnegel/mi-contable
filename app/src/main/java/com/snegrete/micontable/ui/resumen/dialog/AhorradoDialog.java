package com.snegrete.micontable.ui.resumen.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.snegrete.micontable.R;
import com.snegrete.micontable.ui.resumen.listener.ResumenListener;

public class AhorradoDialog extends DialogFragment {

    private ResumenListener resumenListener;

    private String ahorrado;

    public AhorradoDialog(String ahorrado) {
        this.ahorrado = ahorrado;
    }

    public String getAhorro() {
        return ahorrado;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        View dialogLayout = getActivity().getLayoutInflater().inflate(R.layout.dialog_ahorrado, null);

        AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setView(dialogLayout)
                .setCancelable(false)
                .setPositiveButton(R.string.btn_aceptar, getNewButton())
                .setNegativeButton(R.string.btn_cancelar, getCancelButton())
                .create();

        ((EditText)dialogLayout.findViewById(R.id.ahorradoEditText)).setText(ahorrado);
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

    private DialogInterface.OnClickListener getNewButton() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                resumenListener.updateAhorrado(((EditText)((AlertDialog) dialog).findViewById(R.id.ahorradoEditText)).getEditableText().toString());
                dialog.dismiss();
            }
        };
    }

    private DialogInterface.OnClickListener getCancelButton() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        };
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        resumenListener = (ResumenListener) context;
    }
}
