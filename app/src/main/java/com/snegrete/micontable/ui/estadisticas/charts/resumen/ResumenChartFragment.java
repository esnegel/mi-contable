package com.snegrete.micontable.ui.estadisticas.charts.resumen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.snegrete.micontable.R;
import com.snegrete.micontable.ui.estadisticas.charts.common.ChartFragment;
import com.snegrete.micontable.ui.estadisticas.charts.resumen.drawer.ResumenChartDrawer;

public class ResumenChartFragment extends ChartFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.createView(inflater.inflate(R.layout.fragment_chart_resumen, container, false));
    }

    @Override
    public void drawChart() {
        new ResumenChartDrawer(this).configure();
    }
}
