package com.snegrete.micontable.ui.resumen.ahorros.dialog.configuration;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.snegrete.micontable.R;
import com.snegrete.micontable.common.configurationbean.spinner.SpinnerConfiguratorBean;
import com.snegrete.micontable.common.configurationbean.spinner.SpinnerListDTO;
import com.snegrete.micontable.model.Ahorro;
import com.snegrete.micontable.utilities.CalendarUtils;
import com.snegrete.micontable.utilities.ListUtils;

import static android.view.View.GONE;

public class AhorroDialogConfigurator {

    private static AhorroDialogConfigurator ahorroDialogConfigurator = new AhorroDialogConfigurator();

    private AhorroDialogConfigurator() {
    }

    public static AhorroDialogConfigurator getInstance() {
        if (ahorroDialogConfigurator == null) {
            ahorroDialogConfigurator = new AhorroDialogConfigurator();
        }
        return ahorroDialogConfigurator;
    }

    public void configure(View dialog) {
        hideId(dialog);
        configureMonthSpinner(dialog, CalendarUtils.getActualMonth() - 1);
        configureYearSpinner(dialog, 3);
        setTitle(dialog, R.string.new_ahorro_dialog_title);
    }

    public void configure(View dialog, Ahorro ahorro) {
        hideId(dialog);
        configureMonthSpinner(dialog, ahorro.getDateInfo().getMonthNumber() - 1);
        configureYearSpinner(dialog, ListUtils.createYearList().indexOf(ahorro.getYear()));
        setTitle(dialog, R.string.edit_ahorro_dialog_title);

        ((EditText)dialog.findViewById(R.id.addAhorroCantidad)).setText(ahorro.getAmount().toString());
        ((EditText)dialog.findViewById(R.id.addAhorroId)).setText(ahorro.getId().toString());
    }

    private void setTitle(View dialog, int title){
        ((TextView) dialog.findViewById(R.id.addAhorroTitle)).setText(title);
    }

    private void hideId(View dialog){
        dialog.findViewById(R.id.addAhorroId).setVisibility(GONE);
    }

    private void configureMonthSpinner(View dialog, int selected) {
        new SpinnerConfiguratorBean
                (R.id.addAhorroMonth,
                        dialog,
                        new SpinnerListDTO(R.array.months),
                        selected).configure();

    }

    private void configureYearSpinner(View dialog, int selected) {
        new SpinnerConfiguratorBean
                (R.id.addAhorroYear,
                        dialog,
                        new SpinnerListDTO(ListUtils.createYearList()), selected).configure();
    }
}
