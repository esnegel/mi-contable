package com.snegrete.micontable.ui.gastos.repository;

import android.util.Log;

import com.snegrete.micontable.common.repository.RealmRepository;
import com.snegrete.micontable.database.GastoDB;
import com.snegrete.micontable.model.DateInfo;
import com.snegrete.micontable.ui.resumen.service.dto.GastoVariableInfoDTO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.DoubleSummaryStatistics;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

public class GastosRepository extends RealmRepository<GastoDB> {

    private static GastosRepository gastosRepository = new GastosRepository();

    public static GastosRepository getInstance() {
        return gastosRepository;
    }

    private GastosRepository() {
        super(GastoDB.class);
    }

    public List<GastoVariableInfoDTO> findTop3VariablesByDate(DateInfo dateInfo){
        RealmResults<GastoDB> variables = findGastosVariablesByDate(dateInfo);
        if(variables.isEmpty()){
            return new ArrayList<>();
        }

        Map<String, DoubleSummaryStatistics> mapa = variables.stream().collect(Collectors.groupingBy(GastoDB::getCategoria, Collectors.summarizingDouble(GastoDB::getAmount)));

        final List<GastoVariableInfoDTO> gastosVariables = new ArrayList<>();
        mapa.forEach((k, v) -> gastosVariables.add(new GastoVariableInfoDTO(k, v.getSum())));

        return gastosVariables.stream().sorted().collect(Collectors.toList());
    }

    public RealmResults<GastoDB> findGastosVariablesByDate(DateInfo dateInfo){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<GastoDB> gastos = realm.where(GastoDB.class)
                .equalTo("month", dateInfo.getMonth())
                .equalTo("year", dateInfo.getYear())
                .equalTo("esFijo", Boolean.FALSE)
                .findAllSorted("amount", Sort.DESCENDING);
        realm.close();
        return gastos;
    }

    public RealmResults<GastoDB> findInDates(List<DateInfo> dates){
        Realm realm = Realm.getDefaultInstance();
        Map<String, List<DateInfo>> datesMap = dates.stream().collect(Collectors.groupingBy(DateInfo::getYear));
        RealmQuery<GastoDB> q = realm.where(GastoDB.class);
        datesMap.forEach((k, v) -> {
            List<String> arr = v.stream().map(DateInfo::getMonth).collect(Collectors.toList());
            String[] meses = new String[arr.size()];
            for(int i = 0; i < arr.size(); i++){
                meses[i] = (String) arr.get(i);
            }
            q.beginGroup().equalTo("year", k).in("month", meses).endGroup();
            if(datesMap.keySet().stream().collect(Collectors.toList()).indexOf(k) < datesMap.size()-1){
                q.or();
            }
        });
        RealmResults<GastoDB> gastos = q.findAll();
        realm.close();
        return gastos;
    }
}
