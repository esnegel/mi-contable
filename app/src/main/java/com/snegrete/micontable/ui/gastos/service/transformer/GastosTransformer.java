package com.snegrete.micontable.ui.gastos.service.transformer;

import com.snegrete.micontable.database.GastoDB;
import com.snegrete.micontable.model.Gasto;

public class GastosTransformer {

    private static GastosTransformer gastosTransformer = new GastosTransformer();

    private GastosTransformer() {
    }

    public static GastosTransformer getInstance() {
        if (gastosTransformer == null) {
            gastosTransformer = new GastosTransformer();
        }
        return gastosTransformer;
    }

    public GastoDB toGastoDB(Gasto gasto) {
        return new GastoDB(
                gasto.getId(),
                gasto.getAmount(),
                gasto.getMonth(),
                gasto.getYear(),
                gasto.getConcepto(),
                gasto.getCategoria(),
                gasto.isFijo()
        );
    }

    public Gasto toGasto(GastoDB gastoDb) {
        return new Gasto(
                gastoDb.getId(),
                gastoDb.getAmount(),
                gastoDb.getMonth(),
                gastoDb.getYear(),
                gastoDb.getConcepto(),
                gastoDb.getCategoria(),
                gastoDb.isFijo()
        );
    }
}
