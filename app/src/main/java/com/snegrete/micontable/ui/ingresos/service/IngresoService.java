package com.snegrete.micontable.ui.ingresos.service;

import com.snegrete.micontable.database.IngresoDB;
import com.snegrete.micontable.model.DateInfo;
import com.snegrete.micontable.model.Ingreso;
import com.snegrete.micontable.ui.ingresos.repository.IngresosRepository;
import com.snegrete.micontable.ui.ingresos.service.transformer.IngresosTransformer;
import com.snegrete.micontable.utilities.ReverseStream;

import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class IngresoService {

    private static IngresoService ingresoService = new IngresoService();

    private IngresoService() {
    }

    public static IngresoService getInstance() {
        if (ingresoService == null) {
            ingresoService = new IngresoService();
        }
        return ingresoService;
    }

    private IngresosTransformer ingresosTransformer = IngresosTransformer.getInstance();
    private IngresosRepository ingresosRepository = IngresosRepository.getInstance();

    public TreeMap<DateInfo, List<Ingreso>> getIngresos() {
        return new TreeMap(ingresosRepository.findAll().stream()
                .map(db -> ingresosTransformer.toIngreso(db))
                .collect(ReverseStream.reverse())
                .collect(Collectors.groupingBy(Ingreso::getDateInfo)));
    }

    public List<Ingreso> getIngresos(DateInfo dateInfo) {
        return ingresosRepository.findByDate(dateInfo).stream()
                .map(db -> ingresosTransformer.toIngreso(db))
                .collect(Collectors.toList());
    }

    public Double getTotalIngresos(DateInfo dateInfo){
        return ingresosRepository.findByDate(dateInfo).stream()
                .collect(Collectors.summarizingDouble(IngresoDB::getAmount))
                .getSum();
    }

    public void save(Ingreso ingreso) {
        ingresosRepository.save(ingresosTransformer.toIngresoDB(ingreso));
    }

    public void delete(Ingreso ingreso) {
        ingresosRepository.delete(ingreso.getId());
    }
}
