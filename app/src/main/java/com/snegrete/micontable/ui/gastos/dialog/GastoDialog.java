package com.snegrete.micontable.ui.gastos.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.snegrete.micontable.R;
import com.snegrete.micontable.model.Gasto;
import com.snegrete.micontable.ui.gastos.dialog.configuration.GastoDialogConfigurator;
import com.snegrete.micontable.ui.gastos.listener.GastoListener;

public class GastoDialog extends DialogFragment {

    private GastoListener gastoListener;

    private GastoDialogConfigurator gastoDialogConfigurator = GastoDialogConfigurator.getInstance();

    private GastoDialogTransformer gastoDialogTransformer = GastoDialogTransformer.getInstance();

    private DialogInterface.OnDismissListener onDismissListener;

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

    private Gasto gasto;

    public GastoDialog() {
    }

    public GastoDialog(Gasto gasto) {
        this.gasto = gasto;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        View dialogLayout = getActivity().getLayoutInflater().inflate(R.layout.dialog_gasto, null);

        AlertDialog dialog;

        if(gasto == null){
            dialog = new AlertDialog.Builder(getActivity())
                    .setView(dialogLayout)
                    .setCancelable(false)
                    .setPositiveButton(R.string.btn_aceptar, getNewButton())
                    .setNegativeButton(R.string.btn_cancelar, getCancelButton())
                    .create();
            gastoDialogConfigurator.configure(dialogLayout);
        } else {
            dialog =  new AlertDialog.Builder(getActivity())
                    .setView(dialogLayout)
                    .setCancelable(false)
                    .setNeutralButton("Cancelar", getCancelButton())
                    .setPositiveButton(R.string.btn_editar, getEditButton())
                    .setNegativeButton(R.string.btn_eliminar, getDeleteButton())
                    .create();
            gastoDialogConfigurator.configure(dialogLayout, gasto);
        }

        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

    private DialogInterface.OnClickListener getNewButton(){
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                gastoListener.addGasto(gastoDialogTransformer.toGasto((AlertDialog) dialog));
                dialog.dismiss();
            }
        };
    }

    private DialogInterface.OnClickListener getEditButton(){
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                gastoListener.editGasto(gastoDialogTransformer.toGasto((AlertDialog) dialog));
                dialog.dismiss();
            }
        };
    }

    private DialogInterface.OnClickListener getDeleteButton(){
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                gastoListener.deleteGasto(gastoDialogTransformer.toGasto((AlertDialog) dialog));
                dialog.dismiss();
            }
        };
    }

    private DialogInterface.OnClickListener getCancelButton(){
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        };
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        gastoListener = (GastoListener) context;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (onDismissListener != null) {
            onDismissListener.onDismiss(dialog);
        }
    }

    public Gasto getGasto() {
        return gasto;
    }
}
