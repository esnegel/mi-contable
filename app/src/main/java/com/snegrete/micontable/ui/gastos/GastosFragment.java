package com.snegrete.micontable.ui.gastos;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.snegrete.micontable.R;
import com.snegrete.micontable.model.DateInfo;
import com.snegrete.micontable.model.Gasto;
import com.snegrete.micontable.ui.gastos.configuration.GastosFragmentConfigurator;
import com.snegrete.micontable.ui.gastos.listener.GastoListener;
import com.snegrete.micontable.ui.gastos.service.GastoService;

import java.util.List;
import java.util.Map;

public class GastosFragment extends Fragment {

    private GastoListener gastoListener;
    private Map<DateInfo, List<Gasto>> gastos;
    private View view;

    private GastoService gastoService = GastoService.getInstance();
    private GastosFragmentConfigurator gastosFragmentConfigurator = GastosFragmentConfigurator.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_gastos, container, false);
        gastosFragmentConfigurator.configure(this);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        searchAllGastos();
        gastoListener = (GastoListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        gastoListener = null;
    }

    public void onDismissGastoDialog(DialogInterface dialog) {
        gastosFragmentConfigurator.updateTabLayout();
    }

    public GastoListener getGastoListener() {
        return gastoListener;
    }

    public View getView() {
        return view;
    }

    public Map<DateInfo, List<Gasto>> searchAllGastos() {
        gastos = gastoService.getGastosPorFecha();
        return gastos;
    }

    public Map<DateInfo, List<Gasto>> getGastos() {
        return gastos;
    }
}