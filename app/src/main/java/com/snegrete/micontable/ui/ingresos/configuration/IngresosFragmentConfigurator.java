package com.snegrete.micontable.ui.ingresos.configuration;

import android.content.DialogInterface;
import android.util.Log;
import android.view.View;

import androidx.viewpager.widget.ViewPager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.snegrete.micontable.R;
import com.snegrete.micontable.common.TabAmount;
import com.snegrete.micontable.model.DateInfo;
import com.snegrete.micontable.model.Ingreso;
import com.snegrete.micontable.ui.ingresos.IngresosFragment;
import com.snegrete.micontable.ui.ingresos.dialog.IngresoDialog;
import com.snegrete.micontable.ui.ingresos.recycler.IngresoListFragment;
import com.snegrete.micontable.ui.ingresos.viewpager.IngresosViewPagerAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class IngresosFragmentConfigurator {

    private static IngresosFragmentConfigurator ingresosFragmentConfigurator = new IngresosFragmentConfigurator();

    IngresosFragment fragment;

    private IngresosFragmentConfigurator() {
    }

    public static IngresosFragmentConfigurator getInstance() {
        if (ingresosFragmentConfigurator == null) {
            ingresosFragmentConfigurator = new IngresosFragmentConfigurator();
        }
        return ingresosFragmentConfigurator;
    }

    public void configure(final IngresosFragment fragment) {
        this.fragment = fragment;
        configureFloatingButton();
        configureTabLayout();
    }

    private void configureFloatingButton() {
        FloatingActionButton button = fragment.getView().findViewById(R.id.addIngresoButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showIngresoDialog();
            }
        });
    }

    private void showIngresoDialog() {
        IngresoDialog ingresoDialog = new IngresoDialog();
        ingresoDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                fragment.onDismissIngresoDialog(dialog);
            }
        });
        ingresoDialog.show(fragment.getFragmentManager(), "Nuevo IngresoDB");
    }

    private void configureTabLayout() {
        TabLayout tabLayout = fragment.getView().findViewById(R.id.ingresos_meses_tab);

        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setSmoothScrollingEnabled(true);

        List<TabAmount<DateInfo, Ingreso>> tabAmountList = new ArrayList<>();

        fragment.getIngresos().forEach((k, v) -> {
            tabAmountList.add(new TabAmount(k, v));
        });

        ViewPager viewPager = fragment.getView().findViewById(R.id.viewPagerIngresos);
        viewPager.setAdapter(new IngresosViewPagerAdapter(fragment.getChildFragmentManager(), tabAmountList, fragment.getIngresoListener()));
        tabLayout.setupWithViewPager(viewPager, true);

        if(!fragment.getIngresos().isEmpty())
            tabLayout.getTabAt(fragment.getIngresos().size() - 1).select();
    }

    public void updateTabLayout() {
        ViewPager viewPager = fragment.getView().findViewById(R.id.viewPagerIngresos);
        IngresosViewPagerAdapter adapter = (IngresosViewPagerAdapter) viewPager.getAdapter();

        Map<DateInfo, List<Ingreso>> gastos = fragment.searchAllIngresos();

        List<TabAmount<DateInfo, Ingreso>> tabAmountList = new ArrayList<>();

        gastos.forEach((k, v) -> {
            tabAmountList.add(new TabAmount(k, v));
        });
        adapter.setTabAmountList(tabAmountList);
        adapter.setTabAmountList(tabAmountList);
        adapter.notifyDataSetChanged();
    }

}
