package com.snegrete.micontable.ui.ingresos.viewpager;

import android.util.Log;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.snegrete.micontable.common.TabAmount;
import com.snegrete.micontable.model.DateInfo;
import com.snegrete.micontable.model.Ingreso;
import com.snegrete.micontable.ui.ingresos.listener.IngresoListener;
import com.snegrete.micontable.ui.ingresos.recycler.IngresoListFragment;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class IngresosViewPagerAdapter extends FragmentPagerAdapter {

    private IngresoListener ingresoListener;

    private List<TabAmount<DateInfo, Ingreso>> tabAmountList;

    public IngresosViewPagerAdapter(@NonNull FragmentManager fm, List<TabAmount<DateInfo, Ingreso>> tabAmountList, IngresoListener ingresoListener) {
        super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.ingresoListener = ingresoListener;
        this.tabAmountList = tabAmountList;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return IngresoListFragment.newInstance(tabAmountList.get(position).getAmountInfoList(), tabAmountList.get(position).getDateInfo(), ingresoListener);
    }

    @Override
    public int getCount() {
        return tabAmountList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return tabAmountList.get(position).getDateInfo().toString();
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        ((IngresoListFragment) object).update();
        return super.getItemPosition(object);
    }

    public void setTabAmountList(List<TabAmount<DateInfo, Ingreso>> tabAmountList) {
        this.tabAmountList = tabAmountList;
    }
}
