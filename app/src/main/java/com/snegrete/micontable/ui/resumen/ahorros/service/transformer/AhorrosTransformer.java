package com.snegrete.micontable.ui.resumen.ahorros.service.transformer;

import com.snegrete.micontable.database.AhorroDB;
import com.snegrete.micontable.model.Ahorro;

public class AhorrosTransformer {

    private static AhorrosTransformer ingresosTransformer = new AhorrosTransformer();

    private AhorrosTransformer() {
    }

    public static AhorrosTransformer getInstance() {
        if (ingresosTransformer == null) {
            ingresosTransformer = new AhorrosTransformer();
        }
        return ingresosTransformer;
    }

    public AhorroDB toAhorroDB(Ahorro ahorro) {
        return new AhorroDB(
                ahorro.getId(),
                ahorro.getAmount(),
                ahorro.getMonth(),
                ahorro.getYear()
        );
    }

    public Ahorro toAhorro(AhorroDB ahorroDB) {
        return new Ahorro(
                ahorroDB.getId(),
                ahorroDB.getAmount(),
                ahorroDB.getMonth(),
                ahorroDB.getYear()
        );
    }
}
