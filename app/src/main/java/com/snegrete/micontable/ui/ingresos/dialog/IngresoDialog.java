package com.snegrete.micontable.ui.ingresos.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.snegrete.micontable.R;
import com.snegrete.micontable.model.Ingreso;
import com.snegrete.micontable.ui.ingresos.dialog.configuration.IngresoDialogConfigurator;
import com.snegrete.micontable.ui.ingresos.listener.IngresoListener;

public class IngresoDialog extends DialogFragment {

    private IngresoListener ingresoListener;

    private IngresoDialogConfigurator ingresoDialogConfigurator = IngresoDialogConfigurator.getInstance();

    private IngresoDialogTransformer ingresoDialogTransformer = IngresoDialogTransformer.getInstance();

    private DialogInterface.OnDismissListener onDismissListener;

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

    private Ingreso ingreso;

    public IngresoDialog() {
    }

    public IngresoDialog(Ingreso ingreso) {
        this.ingreso = ingreso;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        View dialogLayout = getActivity().getLayoutInflater().inflate(R.layout.dialog_ingreso, null);

        AlertDialog dialog;

        if(ingreso == null){
            dialog = new AlertDialog.Builder(getActivity())
                    .setView(dialogLayout)
                    .setCancelable(false)
                    .setPositiveButton(R.string.btn_aceptar, getNewButton())
                    .setNegativeButton(R.string.btn_cancelar, getCancelButton())
                    .create();
            ingresoDialogConfigurator.configure(dialogLayout);
        } else {
            dialog =  new AlertDialog.Builder(getActivity())
                    .setView(dialogLayout)
                    .setCancelable(false)
                    .setNeutralButton("Cancelar", getCancelButton())
                    .setPositiveButton(R.string.btn_editar, getEditButton())
                    .setNegativeButton(R.string.btn_eliminar, getDeleteButton())
                    .create();
            ingresoDialogConfigurator.configure(dialogLayout, ingreso);
        }

        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

    private DialogInterface.OnClickListener getNewButton(){
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ingresoListener.addIngreso(ingresoDialogTransformer.toIngreso((AlertDialog) dialog));
                dialog.dismiss();
            }
        };
    }

    private DialogInterface.OnClickListener getEditButton(){
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ingresoListener.editIngreso(ingresoDialogTransformer.toIngreso((AlertDialog) dialog));
                dialog.dismiss();
            }
        };
    }

    private DialogInterface.OnClickListener getDeleteButton(){
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ingresoListener.deleteIngreso(ingresoDialogTransformer.toIngreso((AlertDialog) dialog));
                dialog.dismiss();
            }
        };
    }

    private DialogInterface.OnClickListener getCancelButton(){
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        };
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ingresoListener = (IngresoListener) context;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (onDismissListener != null) {
            onDismissListener.onDismiss(dialog);
        }
    }

    public Ingreso getIngreso() {
        return ingreso;
    }
}
