package com.snegrete.micontable.ui.ingresos.repository;

import com.snegrete.micontable.common.repository.RealmRepository;
import com.snegrete.micontable.database.IngresoDB;

public class IngresosRepository extends RealmRepository<IngresoDB> {

    private static IngresosRepository ingresosRepository = new IngresosRepository();

    public static IngresosRepository getInstance() {
        return ingresosRepository;
    }

    private IngresosRepository() {
        super(IngresoDB.class);
        this.ingresosRepository = ingresosRepository;
    }
}
