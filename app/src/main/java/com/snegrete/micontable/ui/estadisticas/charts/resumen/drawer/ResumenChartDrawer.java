package com.snegrete.micontable.ui.estadisticas.charts.resumen.drawer;

import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.snegrete.micontable.R;
import com.snegrete.micontable.model.Ahorro;
import com.snegrete.micontable.model.DateInfo;
import com.snegrete.micontable.model.Gasto;
import com.snegrete.micontable.model.Ingreso;
import com.snegrete.micontable.ui.estadisticas.charts.common.drawer.ChartDrawer;
import com.snegrete.micontable.ui.estadisticas.charts.resumen.ResumenChartFragment;
import com.snegrete.micontable.ui.gastos.service.GastoService;
import com.snegrete.micontable.ui.ingresos.service.IngresoService;
import com.snegrete.micontable.ui.resumen.ahorros.service.AhorroService;
import com.snegrete.micontable.utilities.MoneyUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class ResumenChartDrawer extends ChartDrawer implements SeekBar.OnSeekBarChangeListener, OnChartValueSelectedListener {

    private LineChart chart;

    private TextView tvX;
    private TextView tvY;
    private SeekBar seekBarX;
    private SeekBar seekBarY;

    private IngresoService ingresoService = IngresoService.getInstance();
    private GastoService gastoService = GastoService.getInstance();
    private AhorroService ahorroService = AhorroService.getInstance();

    private TreeMap<DateInfo, List<Ingreso>> ingresos;
    private TreeMap<DateInfo, List<Gasto>> gastos;
    private List<Ahorro> ahorros;

    private DateInfo selected;

    private ValueFormatter moneyFormatter = new ValueFormatter() {
        @Override
        public String getFormattedValue(float value) {
            return MoneyUtils.formatAmount((double)value);
        }
    };

    public ResumenChartDrawer(ResumenChartFragment fragment) {
        super(fragment);
        chart = (LineChart) getView(R.id.chartResumen);
        chart.setOnChartValueSelectedListener(this);

        tvX = (TextView) getView(R.id.tvXMax);
        tvY = (TextView) getView(R.id.tvYMax);
        seekBarX = (SeekBar) getView(R.id.seekBar1);
        seekBarY = (SeekBar) getView(R.id.seekBar2);

        tvX.setVisibility(View.GONE);
        tvY.setVisibility(View.GONE);
        seekBarX.setVisibility(View.GONE);
        seekBarY.setVisibility(View.GONE);

        seekBarX.setOnSeekBarChangeListener(this);
        seekBarY.setOnSeekBarChangeListener(this);

        getView(R.id.infoResumenChart).setVisibility(View.GONE);
    }

    @Override
    public void configure() {
        chart.setDrawGridBackground(false);
        chart.getDescription().setEnabled(false);
        chart.setDrawBorders(false);

        // Disable Right axis
        chart.getAxisRight().setEnabled(false);

        List<DateInfo> months = ingresoService.getIngresos().keySet().stream().collect(Collectors.toList());
        // Axis config
        XAxis xAxis = chart.getXAxis();

        xAxis.setLabelCount(months.size(), true);
        xAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return months.get((int)value).toString();
            }
        });
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(false);
        xAxis.setLabelRotationAngle(90);
        xAxis.setTextColor(primaryColor);

        YAxis yAxis = chart.getAxisLeft();
        yAxis.setEnabled(true);
        yAxis.setDrawAxisLine(false);
        yAxis.setDrawGridLines(false);
        yAxis.setValueFormatter(moneyFormatter);
        yAxis.setTextColor(primaryColor);

        // enable touch gestures
        chart.setTouchEnabled(true);

        // enable scaling and dragging
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        chart.setPinchZoom(true);

        seekBarX.setProgress(20);
        seekBarY.setProgress(100);

        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        chart.resetTracking();

        progress = seekBarX.getProgress();

        tvX.setText(String.valueOf(seekBarX.getProgress()));
        tvY.setText(String.valueOf(seekBarY.getProgress()));


        ArrayList<ILineDataSet> dataSets = new ArrayList<>();

        ingresos = ingresoService.getIngresos();
        gastos = gastoService.getGastosPorFecha();
        ahorros = ahorroService.getAhorros().stream().sorted((o1, o2) -> o1.getDateInfo().compareTo(o2.getDateInfo())).collect(Collectors.toList());

        ArrayList<Entry> ingresoEntries = new ArrayList<>();
        ArrayList<Entry> gastoEntries = new ArrayList<>();
        ArrayList<Entry> capacidadEntries = new ArrayList<>();
        ArrayList<Entry> ahorroEntries = new ArrayList<>();

        ingresos.forEach((k, v) -> {
            int index = ingresos.keySet().stream().collect(Collectors.toList()).indexOf(k);
            Double sumIngresos = v.stream().mapToDouble(Ingreso::getAmount).sum();
            Double sumGastos = gastos.get(k).stream().mapToDouble(Gasto::getAmount).sum();
            ingresoEntries.add(new Entry(index , sumIngresos.floatValue()));
            gastoEntries.add(new Entry(index, sumGastos.floatValue()));
            capacidadEntries.add(new Entry(index, (float)(sumIngresos - sumGastos)));
        });
        ahorros.forEach(a -> ahorroEntries.add(new Entry(ahorros.indexOf(a), a.getAmount().floatValue())));

        dataSets.add(configure(ingresoEntries, "Ingresos", R.color.ingresoChartColor));
        dataSets.add(configure(gastoEntries, "Gastos", R.color.gastoChartColor));
        dataSets.add(configure(capacidadEntries, "Capacidad", R.color.capacidadChartColor));
        dataSets.add(configure(ahorroEntries, "Ahorros", R.color.ahorroChartColor));

        LineData data = new LineData(dataSets);
        chart.setData(data);
        chart.invalidate();
    }

    private LineDataSet configure(ArrayList<Entry> entries, String label, int color){
        float defautlLineWidth = 1f;
        float defautlCircleRadius = 3f;

        LineDataSet dataSet = new LineDataSet(entries, label);
        dataSet.setValueFormatter(moneyFormatter);
        dataSet.setDrawValues(false);
        dataSet.setLineWidth(defautlLineWidth);
        dataSet.setCircleRadius(defautlCircleRadius);

        dataSet.setColor(getColor(color));
        dataSet.setCircleColor(getColor(color));

        return dataSet;
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        Ahorro ahorro = ahorros.get((int)e.getX());
        Double ingresos = this.ingresos.get(ahorro.getDateInfo()).stream().mapToDouble(Ingreso::getAmount).sum();
        Double gastos = this.gastos.get(ahorro.getDateInfo()).stream().mapToDouble(Gasto::getAmount).sum();

        if(ahorro.getDateInfo().equals(selected)){
            getView(R.id.infoResumenChart).setVisibility(
                    getView(R.id.infoResumenChart).getVisibility() == View.GONE ?
                            View.VISIBLE : View.GONE
            );
        }else {
            ((TextView) getView(R.id.infoResumenChartTitle)).setText(ahorro.getDateInfo().toString());
            ((TextView) getView(R.id.infoResumenChartIngresos)).setText("Ingresos: " + MoneyUtils.formatAmount(ingresos));
            ((TextView) getView(R.id.infoResumenChartGastos)).setText("Gastos: " + MoneyUtils.formatAmount(gastos));
            ((TextView) getView(R.id.infoResumenChartCapacidad)).setText("Capacidad: " + MoneyUtils.formatAmount(ingresos - gastos));
            ((TextView) getView(R.id.infoResumenChartAhorro)).setText("Ahorrado: " + MoneyUtils.formatAmount(ahorro.getAmount()));

            getView(R.id.infoResumenChart).setVisibility(View.VISIBLE);

            selected = ahorro.getDateInfo();
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onNothingSelected() {
        getView(R.id.infoResumenChart).setVisibility(View.GONE);
    }
}
