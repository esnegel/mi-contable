package com.snegrete.micontable.ui.ingresos.dialog.configuration;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.snegrete.micontable.R;
import com.snegrete.micontable.common.configurationbean.spinner.SpinnerConfiguratorBean;
import com.snegrete.micontable.common.configurationbean.spinner.SpinnerListDTO;
import com.snegrete.micontable.model.Ingreso;
import com.snegrete.micontable.utilities.CalendarUtils;
import com.snegrete.micontable.utilities.ListUtils;

import static android.view.View.GONE;

public class IngresoDialogConfigurator {

    private static IngresoDialogConfigurator ingresoDialogConfigurator = new IngresoDialogConfigurator();

    private IngresoDialogConfigurator() {
    }

    public static IngresoDialogConfigurator getInstance() {
        if (ingresoDialogConfigurator == null) {
            ingresoDialogConfigurator = new IngresoDialogConfigurator();
        }
        return ingresoDialogConfigurator;
    }

    public void configure(View dialog, Ingreso ingreso) {
        hideId(dialog);
        configureMonthSpinner(dialog, ingreso.getDateInfo().getMonthNumber() - 1);
        configureYearSpinner(dialog, ListUtils.createYearList().indexOf(ingreso.getYear()));
        setTitle(dialog, R.string.edit_ingreso_dialog_title);

        ((EditText)dialog.findViewById(R.id.addIngresoCantidad)).setText(ingreso.getAmount().toString());
        ((EditText)dialog.findViewById(R.id.addIngresoConcepto)).setText(ingreso.getConcepto());
        ((EditText)dialog.findViewById(R.id.addIngresoId)).setText(ingreso.getId().toString());
    }

    public void configure(View dialog) {
        hideId(dialog);
        configureMonthSpinner(dialog,CalendarUtils.getActualMonth() - 1);
        configureYearSpinner(dialog, 3);
        setTitle(dialog, R.string.new_ingreso_dialog_title);
    }

    private void setTitle(View dialog, int title){
        ((TextView) dialog.findViewById(R.id.addIngresoTitle)).setText(title);
    }

    private void hideId(View dialog){
        dialog.findViewById(R.id.addIngresoId).setVisibility(GONE);
    }

    private void configureMonthSpinner(View dialog, int selected) {
        new SpinnerConfiguratorBean
                (R.id.addIngresoMonth,
                        dialog,
                        new SpinnerListDTO(R.array.months),
                        selected).configure();
    }

    private void configureYearSpinner(View dialog, int selected) {
        new SpinnerConfiguratorBean
                (R.id.addIngresoYear,
                        dialog,
                        new SpinnerListDTO(ListUtils.createYearList()),selected).configure();
    }
}
