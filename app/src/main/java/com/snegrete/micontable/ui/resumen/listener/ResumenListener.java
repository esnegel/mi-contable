package com.snegrete.micontable.ui.resumen.listener;

public interface ResumenListener {
    void updateObjetivo(String objetivo);
    void updateAhorrado(String ahorrado);
}
