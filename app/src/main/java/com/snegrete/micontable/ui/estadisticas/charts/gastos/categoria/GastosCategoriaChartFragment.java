package com.snegrete.micontable.ui.estadisticas.charts.gastos.categoria;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.snegrete.micontable.R;
import com.snegrete.micontable.ui.estadisticas.charts.common.ChartFragment;
import com.snegrete.micontable.ui.estadisticas.charts.gastos.categoria.drawer.GastosChartDrawer;

public class GastosCategoriaChartFragment extends ChartFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return super.createView(inflater.inflate(R.layout.fragment_chart_gastos_categoria, container, false));
    }

    @Override
    public void drawChart() {
        new GastosChartDrawer(this).configure();
    }
}
