package com.snegrete.micontable.ui.ingresos;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.snegrete.micontable.R;
import com.snegrete.micontable.model.DateInfo;
import com.snegrete.micontable.model.Ingreso;
import com.snegrete.micontable.ui.ingresos.configuration.IngresosFragmentConfigurator;
import com.snegrete.micontable.ui.ingresos.listener.IngresoListener;
import com.snegrete.micontable.ui.ingresos.service.IngresoService;

import java.util.List;
import java.util.Map;

public class IngresosFragment extends Fragment {

    private IngresoListener ingresoListener;
    private Map<DateInfo, List<Ingreso>> ingresos;
    private View view;

    private IngresoService ingresoService = IngresoService.getInstance();
    private IngresosFragmentConfigurator ingresosFragmentConfigurator = IngresosFragmentConfigurator.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_ingresos, container, false);
        ingresosFragmentConfigurator.configure(this);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        searchAllIngresos();
        ingresoListener = (IngresoListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ingresoListener = null;
    }

    public void onDismissIngresoDialog(DialogInterface dialog){
        ingresosFragmentConfigurator.updateTabLayout();
    }

    public IngresoListener getIngresoListener() {
        return ingresoListener;
    }

    public View getView() {
        return view;
    }

    public Map<DateInfo, List<Ingreso>> searchAllIngresos(){
        ingresos = ingresoService.getIngresos();
        return ingresos;
    }

    public Map<DateInfo, List<Ingreso>> getIngresos() {
        return ingresos;
    }
}
