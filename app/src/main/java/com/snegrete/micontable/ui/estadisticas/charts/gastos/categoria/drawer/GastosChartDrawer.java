package com.snegrete.micontable.ui.estadisticas.charts.gastos.categoria.drawer;

import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet;
import com.snegrete.micontable.R;
import com.snegrete.micontable.common.configurationbean.spinner.SpinnerConfiguratorBean;
import com.snegrete.micontable.common.configurationbean.spinner.SpinnerListDTO;
import com.snegrete.micontable.model.DateInfo;
import com.snegrete.micontable.model.Gasto;
import com.snegrete.micontable.ui.estadisticas.charts.common.drawer.ChartDrawer;
import com.snegrete.micontable.ui.estadisticas.charts.gastos.categoria.GastosCategoriaChartFragment;
import com.snegrete.micontable.ui.gastos.service.GastoService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class GastosChartDrawer extends ChartDrawer {

    private Typeface tfLight = Typeface.SANS_SERIF;

    private RadarChart chart;

    private GastoService gastoService = GastoService.getInstance();
    private DateInfo fromDate;
    private DateInfo toDate;

    private String[] categorias;

    public GastosChartDrawer(GastosCategoriaChartFragment fragment) {
        super(fragment);
        chart = (RadarChart) getView(R.id.chartGastos);
        categorias = getStringArray(R.array.categorias);
    }

    @Override
    public void configure() {
        List<DateInfo> meses;
        if(fromDate == null && toDate == null){
            meses = gastoService.getGastosPorFecha().keySet().stream().collect(Collectors.toList());
            fromDate = meses.get(0);
            toDate = meses.get(meses.size()-1);
        } else {
            meses = fromDate.generateDatesTo(toDate);
        }

        configureFilters(meses.stream().map(DateInfo::toString).collect(Collectors.toList()), meses.size());
        drawChart(fromDate.generateDatesTo(toDate));
    }

    private void drawChart(List<DateInfo> meses){
        chart.setBackgroundColor(Color.WHITE);

        chart.getDescription().setEnabled(false);

        chart.setWebLineWidth(2f);
        chart.setWebColor(primaryColor);
        chart.setWebLineWidthInner(0f);
        chart.setWebColorInner(primaryColor);
        chart.setWebAlpha(100);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // toDate use for it
        MarkerView mv = new RadarMarkerView(getContext(), R.layout.radar_markerview);
        mv.setChartView(chart); // For bounds control
        chart.setMarker(mv); // Set the marker toDate the chart

        setData();

        XAxis xAxis = chart.getXAxis();
        xAxis.setTypeface(tfLight);
        xAxis.setTextSize(9f);
        xAxis.setYOffset(0f);
        xAxis.setXOffset(0f);
        xAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return categorias[(int) value % categorias.length];
            }
        });
        xAxis.setTextColor(primaryColor);
        YAxis yAxis = chart.getYAxis();
        yAxis.setTypeface(tfLight);
        yAxis.setLabelCount(meses.size(), false);
        yAxis.setTextSize(9f);
        yAxis.setAxisMinimum(0f);

        yAxis.setDrawLabels(false);

        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setTypeface(tfLight);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(5f);
        l.setTextColor(primaryColor);
    }

    private void setData() {
        TreeMap<String, List<Gasto>> gastos = gastoService.getGastosPorCategoriaIn(fromDate.generateDatesTo(toDate));
        int max = gastoService.getGastosPorFecha().size();
        TreeMap<String, List<Gasto>> gastosCategoria = gastoService.getGastosPorCategoria();
        chart.getYAxis().setAxisMaximum((float)gastos.values().stream()
                .max((o1, o2) -> {
                    Double d1 = Double.valueOf(o1.stream().filter(g -> !g.getConcepto().contains("Alquiler")).mapToDouble(Gasto::getAmount).sum());
                    Double d2 = Double.valueOf(o2.stream().filter(g -> !g.getConcepto().contains("Alquiler")).mapToDouble(Gasto::getAmount).sum());
                    return d1.compareTo(d2);
                }).get().stream().filter(g -> !g.getConcepto().contains("Alquiler")).mapToDouble(Gasto::getAmount).sum()/ fromDate.generateDatesTo(toDate).size());

        ArrayList<RadarEntry> entries1 = new ArrayList<>();
        ArrayList<RadarEntry> entries2 = new ArrayList<>();
        Arrays.asList(categorias)
                .forEach(c -> {
                    if (gastos.get(c) != null) {
                        Double sum = gastos.get(c).stream().filter(g -> !g.getConcepto().contains("Alquiler")).mapToDouble(Gasto::getAmount).sum();
                        entries1.add(new RadarEntry(sum.floatValue() / fromDate.generateDatesTo(toDate).size()));
                    } else {
                        entries1.add(new RadarEntry(0F));
                    }
                    if (gastosCategoria.get(c) != null) {
                        Double sum2 = gastosCategoria.get(c).stream().filter(g -> !g.getConcepto().contains("Alquiler")).mapToDouble(Gasto::getAmount).sum();
                        entries2.add(new RadarEntry(sum2.floatValue() / max));
                    } else {
                        entries2.add(new RadarEntry(0F));
                    }
                });

        String label = fromDate.equals(toDate) ? fromDate.toString() : fromDate.toString() + " - " + toDate.toString();
        RadarDataSet set1 = new RadarDataSet(entries1, label);
        set1.setColor(primaryColor);
        set1.setFillColor(primaryColor);
        set1.setDrawFilled(true);
        set1.setFillAlpha(100);
        set1.setLineWidth(2f);
        set1.setDrawHighlightCircleEnabled(true);
        set1.setDrawHighlightIndicators(false);

        RadarDataSet set2 = new RadarDataSet(entries2, "Media Global");
        set2.setColor(accentColor);
        set2.setFillColor(accentColor);
        set2.setDrawFilled(true);
        set2.setFillAlpha(0);
        set2.setLineWidth(2f);
        set2.setDrawHighlightCircleEnabled(true);
        set2.setDrawHighlightIndicators(false);


        ArrayList<IRadarDataSet> sets = new ArrayList<>();
        sets.add(set1);
        sets.add(set2);

        RadarData data = new RadarData(sets);
        data.setValueTypeface(tfLight);
        data.setValueTextSize(8f);
        data.setDrawValues(false);
        data.setValueTextColor(Color.WHITE);

        chart.setData(data);
        chart.invalidate();
        chart.animateXY(0, 700, Easing.EaseOutSine);
    }

    private void configureFilters(List<String> meses, int size) {
        new SpinnerConfiguratorBean(R.id.chartGastosDesde, getView(), new SpinnerListDTO(meses), 0).configure();
        new SpinnerConfiguratorBean(R.id.chartGastosHasta, getView(), new SpinnerListDTO(meses), size-1).configure();

        ((Spinner) getView(R.id.chartGastosDesde)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String[] dateInfoStrings = meses.get(position).split(" ");
                fromDate = new DateInfo(dateInfoStrings[0], dateInfoStrings[1]);
                if(fromDate.compareTo(toDate) > 0){
                    ((Spinner) getView(R.id.chartGastosHasta)).setSelection(position, true);
                } else {
                    setData();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ((Spinner) getView(R.id.chartGastosHasta)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String[] dateInfoStrings = meses.get(position).split(" ");
                toDate = new DateInfo(dateInfoStrings[0], dateInfoStrings[1]);
                if(fromDate.compareTo(toDate) > 0){
                    ((Spinner) getView(R.id.chartGastosDesde)).setSelection(position, true);
                } else {
                    setData();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
