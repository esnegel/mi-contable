package com.snegrete.micontable.ui.ingresos.listener;

import android.view.View;

import com.snegrete.micontable.model.Ingreso;

public interface IngresoListener {

    void addIngreso(Ingreso ingreso);
    void editIngreso(Ingreso toIngreso);
    void deleteIngreso(Ingreso toIngreso);

    View.OnLongClickListener onClickIngreso(Ingreso ingreso);
}
