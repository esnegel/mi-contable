package com.snegrete.micontable.ui.ingresos.recycler;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.snegrete.micontable.R;
import com.snegrete.micontable.model.Ingreso;
import com.snegrete.micontable.ui.ingresos.listener.IngresoListener;
import com.snegrete.micontable.utilities.MoneyUtils;

public class IngresoViewHolder extends RecyclerView.ViewHolder {

    private View view;

    private TextView amount;
    private TextView concepto;

    public IngresoViewHolder(View view) {
        super(view);
        this.view = view;
        this.amount = view.findViewById(R.id.gasto_amount);
        this.concepto = view.findViewById(R.id.ingreso_concepto);
    }

    public void bind(Ingreso ingreso, IngresoListener ingresoListener){
        amount.setText(MoneyUtils.formatAmount(ingreso.getAmount()));
        concepto.setText(ingreso.getConcepto());

        view.setOnLongClickListener(ingresoListener.onClickIngreso(ingreso));
    }
}
