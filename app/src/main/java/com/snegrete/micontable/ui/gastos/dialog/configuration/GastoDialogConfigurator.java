package com.snegrete.micontable.ui.gastos.dialog.configuration;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.snegrete.micontable.R;
import com.snegrete.micontable.common.configurationbean.spinner.SpinnerConfiguratorBean;
import com.snegrete.micontable.common.configurationbean.spinner.SpinnerListDTO;
import com.snegrete.micontable.model.Gasto;
import com.snegrete.micontable.utilities.CalendarUtils;
import com.snegrete.micontable.utilities.ListUtils;

import java.util.Arrays;

import static android.view.View.GONE;

public class GastoDialogConfigurator {

    private static GastoDialogConfigurator gastoDialogConfigurator = new GastoDialogConfigurator();

    private GastoDialogConfigurator() {
    }

    public static GastoDialogConfigurator getInstance() {
        if (gastoDialogConfigurator == null) {
            gastoDialogConfigurator = new GastoDialogConfigurator();
        }
        return gastoDialogConfigurator;
    }

    public void configure(View dialog) {
        hideId(dialog);
        Log.i("gasto", "mes " + (CalendarUtils.getActualMonth() - 1));
        configureMonthSpinner(dialog, CalendarUtils.getActualMonth() - 1);
        configureYearSpinner(dialog, 3);
        configureCategoriaSpinner(dialog, 0);
        setTitle(dialog, R.string.new_gasto_dialog_title);
    }

    public void configure(View dialog, Gasto gasto) {
        hideId(dialog);
        configureMonthSpinner(dialog, gasto.getDateInfo().getMonthNumber() - 1);
        configureYearSpinner(dialog, ListUtils.createYearList().indexOf(gasto.getYear()));
        configureCategoriaSpinner(dialog, Arrays.asList(R.array.categorias).indexOf(gasto.getCategoria()));
        setTitle(dialog, R.string.edit_gasto_dialog_title);

        ((EditText)dialog.findViewById(R.id.addGastoCantidad)).setText(gasto.getAmount().toString());
        ((EditText)dialog.findViewById(R.id.addGastoConcepto)).setText(gasto.getConcepto());
        ((Switch)dialog.findViewById(R.id.addGastoEsFijo)).setChecked(gasto.isFijo());
        ((EditText)dialog.findViewById(R.id.addGastoId)).setText(gasto.getId().toString());
    }

    private void setTitle(View dialog, int title){
        ((TextView) dialog.findViewById(R.id.addGastoTitle)).setText(title);
    }

    private void hideId(View dialog){
        dialog.findViewById(R.id.addGastoId).setVisibility(GONE);
    }

    private void configureMonthSpinner(View dialog, int selected) {
        new SpinnerConfiguratorBean
                (R.id.addGastoMonth,
                        dialog,
                        new SpinnerListDTO(R.array.months),
                        selected).configure();
    }

    private void configureYearSpinner(View dialog, int selected) {
        new SpinnerConfiguratorBean
                (R.id.addGastoYear,
                        dialog,
                        new SpinnerListDTO(ListUtils.createYearList()),selected).configure();
    }

    private void configureCategoriaSpinner(View dialog, int selected) {
        new SpinnerConfiguratorBean
                (R.id.addGastoCategoria,
                        dialog,
                        new SpinnerListDTO(R.array.categorias),selected).configure();
    }
}
