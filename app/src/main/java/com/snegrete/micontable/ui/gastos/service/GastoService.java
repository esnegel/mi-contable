package com.snegrete.micontable.ui.gastos.service;

import com.snegrete.micontable.database.GastoDB;
import com.snegrete.micontable.model.DateInfo;
import com.snegrete.micontable.model.Gasto;
import com.snegrete.micontable.ui.gastos.repository.GastosRepository;
import com.snegrete.micontable.ui.gastos.service.transformer.GastosTransformer;
import com.snegrete.micontable.ui.resumen.service.dto.GastoVariableInfoDTO;
import com.snegrete.micontable.utilities.ReverseStream;

import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class GastoService {

    private static GastoService gastoService = new GastoService();

    private GastoService() {
    }

    public static GastoService getInstance() {
        if (gastoService == null) {
            gastoService = new GastoService();
        }
        return gastoService;
    }

    private GastosTransformer gastosTransformer = GastosTransformer.getInstance();
    private GastosRepository gastosRepository = GastosRepository.getInstance();

    public TreeMap<DateInfo, List<Gasto>> getGastosPorFecha() {
        return new TreeMap(gastosRepository.findAll().stream()
                .map(db -> gastosTransformer.toGasto(db))
                .collect(ReverseStream.reverse())
                .collect(Collectors.groupingBy(Gasto::getDateInfo)));
    }

    public TreeMap<String, List<Gasto>> getGastosPorCategoria(){
        return new TreeMap(gastosRepository.findAll().stream()
                .map(db -> gastosTransformer.toGasto(db))
                .collect(ReverseStream.reverse())
                .collect(Collectors.groupingBy(Gasto::getCategoria)));
    }

    public TreeMap<String, List<Gasto>> getGastosPorCategoriaIn(List<DateInfo> dates){
        return new TreeMap(gastosRepository.findInDates(dates).stream()
                .map(db -> gastosTransformer.toGasto(db))
                .collect(ReverseStream.reverse())
                .collect(Collectors.groupingBy(Gasto::getCategoria)));
    }

    public List<Gasto> getGastosPorFecha(DateInfo dateInfo) {
        return gastosRepository.findByDate(dateInfo).stream()
                .map(db -> gastosTransformer.toGasto(db))
                .collect(Collectors.toList());
    }

    public Double getTotalGastos(DateInfo dateInfo){
        return gastosRepository.findByDate(dateInfo).stream()
                .collect(Collectors.summarizingDouble(GastoDB::getAmount))
                .getSum();
    }

    public Double getGastosVariables(DateInfo dateInfo){
        return gastosRepository.findGastosVariablesByDate(dateInfo).stream()
                .collect(Collectors.summarizingDouble(GastoDB::getAmount))
                .getSum();
    }

    public List<GastoVariableInfoDTO> getTopGastosVariables(DateInfo actualDateInfo) {
        return gastosRepository.findTop3VariablesByDate(actualDateInfo);
    }

    public void save(Gasto gasto) {
        gastosRepository.save(gastosTransformer.toGastoDB(gasto));
    }

    public void delete(Gasto gasto) {
        gastosRepository.delete(gasto.getId());
    }
}
