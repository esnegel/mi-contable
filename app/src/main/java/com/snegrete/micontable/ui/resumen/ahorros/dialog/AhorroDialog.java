package com.snegrete.micontable.ui.resumen.ahorros.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.snegrete.micontable.R;
import com.snegrete.micontable.model.Ahorro;
import com.snegrete.micontable.ui.resumen.ahorros.dialog.configuration.AhorroDialogConfigurator;
import com.snegrete.micontable.ui.resumen.ahorros.listener.AhorroListener;

public class AhorroDialog extends DialogFragment {

    private AhorroListener ahorroListener;

    private AhorroDialogConfigurator ahorroDialogConfigurator = AhorroDialogConfigurator.getInstance();

    private AhorroDialogTransformer ahorroDialogTransformer = AhorroDialogTransformer.getInstance();

    private DialogInterface.OnDismissListener onDismissListener;

    private Ahorro ahorro;

    public AhorroDialog() {
    }

    public AhorroDialog(Ahorro ahorro) {
        this.ahorro = ahorro;
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        View dialogLayout = getActivity().getLayoutInflater().inflate(R.layout.dialog_ahorro, null);

        AlertDialog dialog;

        if(ahorro == null){
            dialog = new AlertDialog.Builder(getActivity())
                    .setView(dialogLayout)
                    .setCancelable(false)
                    .setPositiveButton(R.string.btn_aceptar, getNewButton())
                    .setNegativeButton(R.string.btn_cancelar, getCancelButton())
                    .create();
            ahorroDialogConfigurator.configure(dialogLayout);
        } else {
            dialog =  new AlertDialog.Builder(getActivity())
                    .setView(dialogLayout)
                    .setCancelable(false)
                    .setNeutralButton("Cancelar", getCancelButton())
                    .setPositiveButton(R.string.btn_editar, getEditButton())
                    .setNegativeButton(R.string.btn_eliminar, getDeleteButton())
                    .create();

            ahorroDialogConfigurator.configure(dialogLayout, ahorro);
        }

        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

    private DialogInterface.OnClickListener getNewButton(){
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ahorroListener.addAhorro(ahorroDialogTransformer.toAhorro((AlertDialog) dialog));
                dialog.dismiss();
            }
        };
    }

    private DialogInterface.OnClickListener getEditButton(){
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ahorroListener.editAhorro(ahorroDialogTransformer.toAhorro((AlertDialog) dialog));
                dialog.dismiss();
            }
        };
    }

    private DialogInterface.OnClickListener getDeleteButton(){
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ahorroListener.deleteAhorro(ahorroDialogTransformer.toAhorro((AlertDialog) dialog));
                dialog.dismiss();
            }
        };
    }

    private DialogInterface.OnClickListener getCancelButton(){
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        };
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ahorroListener = (AhorroListener) context;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (onDismissListener != null) {
            onDismissListener.onDismiss(dialog);
        }
    }

    public Ahorro getAhorro() {
        return ahorro;
    }
}
