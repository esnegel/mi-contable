package com.snegrete.micontable.ui.gastos.recycler;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.snegrete.micontable.R;
import com.snegrete.micontable.model.Gasto;
import com.snegrete.micontable.ui.gastos.listener.GastoListener;
import com.snegrete.micontable.utilities.MoneyUtils;

public class GastoViewHolder extends RecyclerView.ViewHolder {

    private View view;

    private TextView amount;
    private TextView concepto;
    private TextView categoria;
    private ImageView esFijo;

    public GastoViewHolder(View view) {
        super(view);
        this.view = view;
        this.amount = view.findViewById(R.id.gasto_amount);
        this.concepto = view.findViewById(R.id.gasto_concepto);
        this.categoria = view.findViewById(R.id.gasto_categoria);
        this.esFijo = view.findViewById(R.id.esGastoFijoIcon);
    }

    public void bind(Gasto gasto, GastoListener gastoListener){
        amount.setText(MoneyUtils.formatAmount(gasto.getAmount()));
        concepto.setText(gasto.getConcepto());
        categoria.setText(gasto.getCategoria());
        esFijo.setVisibility(gasto.isFijo() ? View.VISIBLE : View.GONE);

        view.setOnLongClickListener(gastoListener.onClickGasto(gasto));
    }
}
