package com.snegrete.micontable.ui.resumen.ahorros.repository;

import com.snegrete.micontable.common.repository.RealmRepository;
import com.snegrete.micontable.database.AhorroDB;

public class AhorrosRepository extends RealmRepository<AhorroDB> {

    private static AhorrosRepository ahorrosRepository = new AhorrosRepository();

    public static AhorrosRepository getInstance() {
        return ahorrosRepository;
    }

    private AhorrosRepository() {
        super(AhorroDB.class);
        this.ahorrosRepository = ahorrosRepository;
    }
}
