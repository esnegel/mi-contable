package com.snegrete.micontable.ui.estadisticas.charts.common;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public abstract class ChartFragment extends Fragment {

    private View view;

    @Nullable
    @Override
    public View getView() {
        return view;
    }

    @Nullable
    @Override
    public abstract View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState);

    public View createView(View view) {
        this.view = view;
        drawChart();
        return view;
    }

    public abstract void drawChart();
}
