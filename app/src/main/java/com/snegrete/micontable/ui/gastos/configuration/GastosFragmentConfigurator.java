package com.snegrete.micontable.ui.gastos.configuration;

import android.content.DialogInterface;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.snegrete.micontable.R;
import com.snegrete.micontable.app.MiContable;
import com.snegrete.micontable.common.TabAmount;
import com.snegrete.micontable.model.DateInfo;
import com.snegrete.micontable.model.Gasto;
import com.snegrete.micontable.ui.gastos.GastosFragment;
import com.snegrete.micontable.ui.gastos.dialog.GastoDialog;
import com.snegrete.micontable.ui.gastos.recycler.GastoListFragment;
import com.snegrete.micontable.ui.gastos.viewpager.GastosViewPagerAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GastosFragmentConfigurator {

    private static GastosFragmentConfigurator gastosFragmentConfigurator = new GastosFragmentConfigurator();

    GastosFragment fragment;

    private GastosFragmentConfigurator() {
    }

    public static GastosFragmentConfigurator getInstance() {
        if (gastosFragmentConfigurator == null) {
            gastosFragmentConfigurator = new GastosFragmentConfigurator();
        }
        return gastosFragmentConfigurator;
    }

    public void configure(final GastosFragment fragment) {
        this.fragment = fragment;
        configureFloatingButton();
        configureTabLayout();
    }

    private void configureFloatingButton() {
        FloatingActionButton button = fragment.getView().findViewById(R.id.addGastoButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showGastoDialog();
            }
        });
    }

    private void showGastoDialog() {
        GastoDialog gastoDialog = new GastoDialog();
        gastoDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                fragment.onDismissGastoDialog(dialog);
            }
        });
        gastoDialog.show(fragment.getFragmentManager(), "Nuevo GastoDB");
    }

    private void configureTabLayout() {
        TabLayout tabLayout = fragment.getView().findViewById(R.id.gastos_meses_tab);

        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setSmoothScrollingEnabled(true);

        List<TabAmount<DateInfo, Gasto>> tabAmountList = new ArrayList<>();

        fragment.getGastos().forEach((k, v) -> {
            tabAmountList.add(new TabAmount(k, v));
        });

        ViewPager viewPager = fragment.getView().findViewById(R.id.viewPagerGastos);
        viewPager.setAdapter(new GastosViewPagerAdapter(fragment.getChildFragmentManager(), tabAmountList, fragment.getGastoListener()));
        tabLayout.setupWithViewPager(viewPager, true);

        if (!fragment.getGastos().isEmpty())
            tabLayout.getTabAt(fragment.getGastos().size() - 1).select();
    }

    public void updateTabLayout() {
        ViewPager viewPager = fragment.getView().findViewById(R.id.viewPagerGastos);
        GastosViewPagerAdapter adapter = (GastosViewPagerAdapter) viewPager.getAdapter();

        Map<DateInfo, List<Gasto>> gastos = fragment.searchAllGastos();

        List<TabAmount<DateInfo, Gasto>> tabAmountList = new ArrayList<>();

        gastos.forEach((k, v) -> {
            tabAmountList.add(new TabAmount(k, v));
        });
        adapter.setTabAmountList(tabAmountList);
        adapter.notifyDataSetChanged();

        if (!fragment.getGastos().isEmpty())
            ((TabLayout) fragment.getView().findViewById(R.id.gastos_meses_tab))
                    .getTabAt(fragment.getGastos().size() - 1).select();
    }

}
