package com.snegrete.micontable.ui.resumen.ahorros.dialog;


import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AlertDialog;

import com.snegrete.micontable.R;
import com.snegrete.micontable.model.Ahorro;
import com.snegrete.micontable.model.Ingreso;

public class AhorroDialogTransformer {

    private static AhorroDialogTransformer ahorroDialogTransformer = new AhorroDialogTransformer();

    private AhorroDialogTransformer(){
    }

    public static AhorroDialogTransformer getInstance(){
        if(ahorroDialogTransformer == null){
            ahorroDialogTransformer = new AhorroDialogTransformer();
        }
        return ahorroDialogTransformer;
    }

    private static final int ID_CANTIDAD = R.id.addAhorroCantidad;
    private static final int ID_MONTH = R.id.addAhorroMonth;
    private static final int ID_YEAR = R.id.addAhorroYear;
    private static final int ID_ID = R.id.addAhorroId;

    public Ahorro toAhorro(AlertDialog ahorroDialog){
        String cantidad = getTextByID(ahorroDialog, ID_CANTIDAD);
        String month = getSpinnerTextByID(ahorroDialog, ID_MONTH);
        String year = getSpinnerTextByID(ahorroDialog, ID_YEAR);
        String id = getTextByID(ahorroDialog, ID_ID);

        return new Ahorro(
                "".equals(id) ? null : Long.valueOf(id),
                "".equals(cantidad) ? Double.NaN : Double.valueOf(cantidad),
                month,
                year);
    }

    private String getTextByID(AlertDialog dialog, int id){
        return ((EditText)(dialog.findViewById(id))).getEditableText().toString();
    }

    private String getSpinnerTextByID(AlertDialog dialog, int id){
        return ((Spinner)(dialog.findViewById(id))).getSelectedItem().toString();
    }
}
