package com.snegrete.micontable.ui.ingresos.dialog;


import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AlertDialog;

import com.snegrete.micontable.R;
import com.snegrete.micontable.model.Ingreso;

public class IngresoDialogTransformer {

    private static IngresoDialogTransformer ingresoDialogTransformer = new IngresoDialogTransformer();

    private IngresoDialogTransformer(){
    }

    public static IngresoDialogTransformer getInstance(){
        if(ingresoDialogTransformer == null){
            ingresoDialogTransformer = new IngresoDialogTransformer();
        }
        return ingresoDialogTransformer;
    }

    private static final int ID_CANTIDAD = R.id.addIngresoCantidad;
    private static final int ID_MONTH = R.id.addIngresoMonth;
    private static final int ID_YEAR = R.id.addIngresoYear;
    private static final int ID_CONCEPTO = R.id.addIngresoConcepto;
    private static final int ID_ID = R.id.addIngresoId;

    public Ingreso toIngreso(AlertDialog ingresoDialog){
        String cantidad = getTextByID(ingresoDialog, ID_CANTIDAD);
        String concepto = getTextByID(ingresoDialog, ID_CONCEPTO);
        String id = getTextByID(ingresoDialog, ID_ID);
        String month = getSpinnerTextByID(ingresoDialog, ID_MONTH);
        String year = getSpinnerTextByID(ingresoDialog, ID_YEAR);

        return new Ingreso(
                "".equals(id) ? null : Long.valueOf(id),
                "".equals(cantidad) ? Double.NaN : Double.valueOf(cantidad),
                month,
                year,
                concepto);
    }

    private String getTextByID(AlertDialog dialog, int id){
        return ((EditText)(dialog.findViewById(id))).getEditableText().toString();
    }

    private String getSpinnerTextByID(AlertDialog dialog, int id){
        return ((Spinner)(dialog.findViewById(id))).getSelectedItem().toString();
    }
}
