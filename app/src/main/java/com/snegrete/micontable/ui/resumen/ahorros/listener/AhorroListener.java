package com.snegrete.micontable.ui.resumen.ahorros.listener;

import android.view.View;

import com.snegrete.micontable.model.Ahorro;

public interface AhorroListener {

    void addAhorro(Ahorro ahorro);
    void editAhorro(Ahorro ahorro);
    void deleteAhorro(Ahorro ahorro);

    View.OnLongClickListener onClickAhorro(Ahorro ahorro);
}
