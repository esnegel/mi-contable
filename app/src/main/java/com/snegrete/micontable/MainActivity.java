package com.snegrete.micontable;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.snegrete.micontable.app.MiContable;
import com.snegrete.micontable.configuration.MainActivityConfigurator;
import com.snegrete.micontable.database.AhorroDB;
import com.snegrete.micontable.database.GastoDB;
import com.snegrete.micontable.database.IngresoDB;
import com.snegrete.micontable.database.ResumenDB;
import com.snegrete.micontable.model.Ahorro;
import com.snegrete.micontable.model.Gasto;
import com.snegrete.micontable.model.Ingreso;
import com.snegrete.micontable.ui.gastos.GastosFragment;
import com.snegrete.micontable.ui.gastos.dialog.GastoDialog;
import com.snegrete.micontable.ui.gastos.listener.GastoListener;
import com.snegrete.micontable.ui.gastos.recycler.GastoListFragment;
import com.snegrete.micontable.ui.gastos.service.GastoService;
import com.snegrete.micontable.ui.ingresos.IngresosFragment;
import com.snegrete.micontable.ui.ingresos.dialog.IngresoDialog;
import com.snegrete.micontable.ui.ingresos.listener.IngresoListener;
import com.snegrete.micontable.ui.ingresos.service.IngresoService;
import com.snegrete.micontable.ui.resumen.ResumenFragment;
import com.snegrete.micontable.ui.resumen.ahorros.AhorroFragment;
import com.snegrete.micontable.ui.resumen.ahorros.dialog.AhorroDialog;
import com.snegrete.micontable.ui.resumen.ahorros.listener.AhorroListener;
import com.snegrete.micontable.ui.resumen.ahorros.service.AhorroService;
import com.snegrete.micontable.ui.resumen.listener.ResumenListener;
import com.snegrete.micontable.ui.resumen.service.ResumenService;
import com.snegrete.micontable.utilities.FragmentUtils;
import com.snegrete.micontable.validation.AmountInfoValidator;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;

import static com.snegrete.micontable.utilities.ToastUtils.showDefaultToast;

public class MainActivity extends AppCompatActivity implements IngresoListener, GastoListener, AhorroListener, ResumenListener {

    private MainActivityConfigurator configurator = MainActivityConfigurator.getInstance(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configurator.configure();
    }

    @Override
    public void addIngreso(Ingreso ingreso) {
        AmountInfoValidator amountInfoValidator = AmountInfoValidator.getInstance();
        if (amountInfoValidator.validate(ingreso)) {
            IngresoService ingresoService = IngresoService.getInstance();
            ingreso.setId(MiContable.ingresoID.incrementAndGet());
            ingresoService.save(ingreso);
            showDefaultToast(this, R.string.ingreso_creado);
        } else {
            showDefaultToast(this, amountInfoValidator.getErrorMsgId());
        }
    }

    @Override
    public void editIngreso(Ingreso ingreso) {
        Log.i("Ingreso", ingreso.getMonth());
        AmountInfoValidator amountInfoValidator = AmountInfoValidator.getInstance();
        if (amountInfoValidator.validate(ingreso)) {
            IngresoService ingresoService = IngresoService.getInstance();
            ingresoService.save(ingreso);
            showDefaultToast(this, R.string.ingreso_editado);
        } else {
            showDefaultToast(this, amountInfoValidator.getErrorMsgId());
        }
    }

    @Override
    public void deleteIngreso(Ingreso ingreso) {
        IngresoService ingresoService = IngresoService.getInstance();
        ingresoService.delete(ingreso);
        showDefaultToast(this, R.string.ingreso_eliminado);
    }

    @Override
    public View.OnLongClickListener onClickIngreso(Ingreso ingreso) {
        return new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                IngresoDialog ingresoDialog = new IngresoDialog(ingreso);
                ingresoDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        new FragmentUtils<IngresosFragment>(NavHostFragment.class)
                                .getFragmentOfType(getSupportFragmentManager())
                                .onDismissIngresoDialog(dialog);
                    }
                });
                ingresoDialog.show(getSupportFragmentManager(), "Editar IngresoDB");
                return true;
            }
        };
    }

    @Override
    public void addAhorro(Ahorro ahorro) {
        AmountInfoValidator amountInfoValidator = AmountInfoValidator.getInstance();
        if (amountInfoValidator.validate(ahorro)) {
            AhorroService ahorroService = AhorroService.getInstance();
            ahorro.setId(MiContable.ahorroID.incrementAndGet());
            ahorroService.save(ahorro);
            showDefaultToast(this, R.string.ahorro_creado);
        } else {
            showDefaultToast(this, amountInfoValidator.getErrorMsgId());
        }
    }

    @Override
    public void editAhorro(Ahorro ahorro) {
        AmountInfoValidator amountInfoValidator = AmountInfoValidator.getInstance();
        if (amountInfoValidator.validateEdit(ahorro)) {
            AhorroService ahorroService = AhorroService.getInstance();
            ahorroService.save(ahorro);
            showDefaultToast(this, R.string.ahorro_editado);
        } else {
            showDefaultToast(this, amountInfoValidator.getErrorMsgId());
        }
    }

    @Override
    public void deleteAhorro(Ahorro ahorro) {
        AhorroService ahorroService = AhorroService.getInstance();
        ahorroService.delete(ahorro);
        showDefaultToast(this, R.string.ahorro_eliminado);
    }

    @Override
    public View.OnLongClickListener onClickAhorro(Ahorro ahorro) {
        return new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AhorroDialog ahorroDialog = new AhorroDialog(ahorro);
                ahorroDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        new FragmentUtils<AhorroFragment>(NavHostFragment.class, ResumenFragment.class)
                                .getFragmentOfType(getSupportFragmentManager())
                                .onDismissAhorroDialog(dialog);
                    }
                });
                ahorroDialog.show(getSupportFragmentManager(), "Editar AhorroDB");
                return true;
            }
        };
    }

    @Override
    public void addGasto(Gasto gasto) {
        AmountInfoValidator amountInfoValidator = AmountInfoValidator.getInstance();
        if (amountInfoValidator.validate(gasto)) {
            GastoService gastoService = GastoService.getInstance();
            gasto.setId(MiContable.gastoID.incrementAndGet());
            gastoService.save(gasto);
            showDefaultToast(this, R.string.gasto_creado);
        } else {
            showDefaultToast(this, amountInfoValidator.getErrorMsgId());
        }
    }

    @Override
    public void editGasto(Gasto gasto) {
        AmountInfoValidator amountInfoValidator = AmountInfoValidator.getInstance();
        if (amountInfoValidator.validate(gasto)) {
            GastoService gastoService = GastoService.getInstance();
            gastoService.save(gasto);
            showDefaultToast(this, R.string.gasto_editado);
        } else {
            showDefaultToast(this, amountInfoValidator.getErrorMsgId());
        }
    }

    @Override
    public void deleteGasto(Gasto gasto) {
        GastoService gastoService = GastoService.getInstance();
        gastoService.delete(gasto);
        showDefaultToast(this, R.string.gasto_eliminado);
    }

    @Override
    public View.OnLongClickListener onClickGasto(Gasto gasto) {
        return new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                GastoDialog gastoDialog = new GastoDialog(gasto);
                gastoDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        new FragmentUtils<GastosFragment>(NavHostFragment.class)
                                .getFragmentOfType(getSupportFragmentManager())
                                .onDismissGastoDialog(dialog);
                    }
                });
                gastoDialog.show(getSupportFragmentManager(), "Editar GastoDB");
                return true;
            }
        };
    }

    @Override
    public void updateObjetivo(String objetivo) {
        ResumenService resumenServcie = ResumenService.getInstance();
        resumenServcie.saveObjetivo(Double.valueOf(objetivo));
        updateResumen();
        showDefaultToast(this, R.string.objetivo_actualizado);
    }

    @Override
    public void updateAhorrado(String ahorrado) {
        ResumenService resumenServcie = ResumenService.getInstance();
        resumenServcie.saveAhorrado(Double.valueOf(ahorrado));
        updateResumen();
        showDefaultToast(this, R.string.objetivo_actualizado);
    }

    private void updateResumen() {
        new FragmentUtils<ResumenFragment>(NavHostFragment.class)
                .getFragmentOfType(getSupportFragmentManager()).updateCardViews();
    }
}
