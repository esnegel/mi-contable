package com.snegrete.micontable.utilities;

import android.widget.Toast;

import com.snegrete.micontable.MainActivity;
import com.snegrete.micontable.R;

public class ToastUtils {

    private ToastUtils(){}

    public static void showDefaultToast(MainActivity context, int id){
        Toast.makeText(context, id, Toast.LENGTH_LONG).show();
    }
}
