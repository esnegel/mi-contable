package com.snegrete.micontable.utilities;

import android.content.Context;

import com.snegrete.micontable.R;
import com.snegrete.micontable.model.DateInfo;

import java.util.Calendar;
import java.util.Date;

public class CalendarUtils {

    private CalendarUtils(){}

    public static Integer getActualMonth(){
        return getCalendar().get(Calendar.MONTH) + 1;
    }

    public static Integer getActualYear(){
        return getCalendar().get(Calendar.YEAR);
    }

    public static String addMonthsToActualDate(Integer months, Context context){
        Calendar cal = getCalendar();
        cal.add(Calendar.MONTH, months);
        return getMonthText(context, cal) + " " + cal.get(Calendar.YEAR);
    }

    public static DateInfo getActualDateInfo(){
        return new DateInfo(getActualMonth(), getActualYear().toString());
    }

    private static Calendar getCalendar(){
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        return cal;
    }

    private static String getMonthText(Context context, Calendar cal){
        return context.getResources().getStringArray(R.array.months)[cal.get(Calendar.MONTH)];
    }
}
