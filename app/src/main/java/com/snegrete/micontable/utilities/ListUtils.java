package com.snegrete.micontable.utilities;

import java.util.ArrayList;
import java.util.List;

public class ListUtils {

    private ListUtils(){
    }

    public static List<String> createYearList(){
        List<String> years = new ArrayList<>();
        Integer actualYear = CalendarUtils.getActualYear();
        for(Integer year = actualYear- 3; year <= actualYear+3; year++){
            years.add(year.toString());
        }
        return years;
    }
}
