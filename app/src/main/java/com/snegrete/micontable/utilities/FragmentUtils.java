package com.snegrete.micontable.utilities;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class FragmentUtils<T extends Fragment> {

    private List<Class<? extends Fragment>> parents;

    public FragmentUtils(Class<? extends Fragment>... parents) {
        this.parents = Arrays.asList(parents);
    }

    public T getFragmentOfType(FragmentManager fm) {
        AtomicReference<T> fragment = new AtomicReference<>();
        getChildFragmentManager(fm, 0).getFragments().forEach(f -> {
            try {
                fragment.set((T) f);
            } catch (ClassCastException e){
                // El fragment no es de la clase solicitada
            }
        });
        return fragment.get();
    }

    private FragmentManager getChildFragmentManager(FragmentManager fm, int index) {
        AtomicReference<FragmentManager> ret = new AtomicReference<>();

        for (Fragment f : fm.getFragments()) {
            if (parents.get(index).isAssignableFrom(f.getClass())) {
                try {
                    T fT = (T) f;
                    if (parents.size() - 1 > index) {
                        ret.set(getChildFragmentManager(fT.getChildFragmentManager(), ++index));
                    } else {
                        ret.set(fT.getChildFragmentManager());
                    }
                } catch (ClassCastException e){
                    // El fragment no es de la clase solicitada
                }
            }
        }

        return ret.get();
    }
}
