package com.snegrete.micontable.utilities;

import java.text.DecimalFormat;

public class MoneyUtils {

    private MoneyUtils(){}

    public static String formatAmount(Double amount){
        return new DecimalFormat( "#,###,###,##0.00" ).format(amount) + " €";
    }

    public static String formatAmount(Integer amount){
        return new DecimalFormat( "#,###,###,##0.00" ).format(amount) + " €";
    }
}
